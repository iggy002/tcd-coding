



# This was working out the key value of the largest object stored in a dictanory  
test = Assignment_3[0][:10]
a_dictionary = {"a": 1, "b": 2, "c": 3}

# get key with max value


#len(DataFrame.index)
wow = data_3.groupby(' country').apply(lambda x: x.to_dict(orient='r')).to_dict()


lista = [len(v) for v in wow.values()]

dictionary = dict(zip(wow.keys(), lista))
answer = max(dictionary , key=dictionary .get)

def keywithmaxval(d):
     """ a) create a list of the dict's keys and values; 
         b) return the key with the max value"""  
     v=list(d.values())
     k=list(d.keys())
     return k[v.index(max(v))]
 
    
from collections import OrderedDict

def get_top_players(data, n=10, order=False):
    """Get top n players by score. 

    Returns a dictionary or an `OrderedDict` if `order` is true.
    """ 
    top = sorted(data.items(), key=lambda x: x[1], reverse=True)[:n]
    if order:
        return OrderedDict(top)
    return dict(top)


new_dict = dict(zip(keys, values))


## STOREING STUFF 
"""
#Assignment_3_b 
base =data_3
# filter by cities 
countries = base[' country'].unique() 
trick = base[' country'].unique()

# dic , with keys being the name of each country, keys store dataframe with all the entries for that country 
countries_dict = dict()


# Get list of ID's for each 'Category'
for i in range (len( trick)):
    countries[i] = pd.DataFrame(base.loc[base[' country'] == trick [i]] ).size

for i in  range (len(trick)):
    n= trick[i]
    countries_dict[n] = countries [i]


(
    wine.rename(columns={"color_intensity": "ci"})
    .assign(color_filter=lambda x: np.where((x.hue > 1) & (x.ci > 7), 1, 0)) # this line is of interest 
    .query("alcohol > 14 and color_filter == 1")
    .sort_values("alcohol", ascending=False)
    .reset_index(drop=True)
    .loc[:, ["alcohol", "ci", "hue"]]
)

"""

# 10 cut function for binning 

#Binning:
def binning(col, cut_points, labels=None):
  #Define min and max values:
  minval = col.min()
  maxval = col.max()

  #create list by adding min and max to cut_points
  break_points = [minval] + cut_points + [maxval]

  #if no labels provided, use default labels 0 ... (n-1)
  if not labels:
    labels = range(len(cut_points)+1)

  #Binning using cut function of pandas
  colBin = pd.cut(col,bins=break_points,labels=labels,include_lowest=True)
  return colBin

#Binning age:
cut_points = [90,140,190]
labels = ["low","medium","high","very high"]
data["LoanAmount_Bin"] = binning(data["LoanAmount"], cut_points, labels)
print pd.value_counts(data["LoanAmount_Bin"], sort=False)


#Define a generic function using Pandas replace function
def coding(col, codeDict):
  colCoded = pd.Series(col, copy=True)
  for key, value in codeDict.items():
    colCoded.replace(key, value, inplace=True)
  return colCoded
 
#Coding LoanStatus as Y=1, N=0:
print 'Before Coding:'
print pd.value_counts(data["Loan_Status"])
data["Loan_Status_Coded"] = coding(data["Loan_Status"], {'N':0,'Y':1})
print '\nAfter Coding:'
print pd.value_counts(data["Loan_Status_Coded"])