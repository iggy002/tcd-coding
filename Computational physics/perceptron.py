import numpy as np
import matplotlib.pyplot as plt


class Perceptron:
    def __init__(selfy, N):
        selfy.N = N
        selfy.weight = np.random.uniform(-1, 1, selfy.N)
        selfy.neuron = np.sign(np.random.randint(0, 2, selfy.N))
        selfy.neuron[selfy.neuron == 0] = -1

    def predict(selfy, x):
        h = np.sign(np.sum(selfy.weight * selfy.neuron))
        x = np.sign(x)
        if (h != x):
            selfy.weight += float(x) * selfy.neuron / float(selfy.N) #punish wrong neurons using the fact we know the signs
        
        print(selfy.weight)
        selfy.neuron[1:] = selfy.neuron[:-1]
        selfy.neuron[0] = x
        return h == x
