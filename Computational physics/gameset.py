import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button
import perceptron

run = 0
N = 10
Per = perceptron.Perceptron(N)
acc = []
facc = []

def update_left(event):
    update_all(1)

def update_right(event):
    update_all(-1)

def update_all(x):

    global acc
    global facc

    result = Per.predict(x)

    if result:
        acc.append(1)
        print("I got it right!")
    else:
        acc.append(0)
        print("I got it wrong")

    
    facc.append(float(np.sum(acc))/float(len(acc))*100)

    ac.set_data(range(len(facc)),facc)

    for rect, height in zip(weight, Per.weight):
        if height > 0:
            rect.set_height(height)
        else:
            rect.set_height(height)
            rect.set_y(0)
            print("set")

    fig.canvas.draw()

fig, (ax1, ax2) = plt.subplots(nrows=2)
plt.subplots_adjust(bottom=0.2)

weight = ax1.bar(range(10), Per.weight, bottom=0)
ax1.set_ylim(-1.2, 1.2)
ax1.set_title("Weights vs Weight Number")
ax1.set_ylabel("Weight")

ac, = ax2.plot(acc, 'r', lw=3)
ax2.set_xlim(0, 100)
ax2.set_ylim(0, 100)
ax2.plot(range(100),np.repeat(50,100), '--')
ax2.set_xlabel("Number of Trys")
ax2.set_ylabel("Percentage of Correct Predictions")

axleft = plt.axes([0.4, 0.04, 0.1, 0.075])
axright = plt.axes([0.51, 0.04, 0.1, 0.075])

bleft = Button(axleft, 'Left')
bright = Button(axright, 'Right')

bleft.on_clicked(update_left)
bright.on_clicked(update_right)
#plt.tight_layout()
plt.show()




