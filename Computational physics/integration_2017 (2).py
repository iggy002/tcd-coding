# code for PY3C01-numerical methods
# integration, September 2014

# aim:
# numerical integration using Python
# rectangular and trapezoid method
# computation of relative errors
# example integral_0^1 exp(x) = e-1

import numpy as np
import matplotlib.pyplot as plt


def function(x):
    """ define function to be integrated"""
    f= np.exp(x)
    return f

def analytic(x):
    """analytic integral for integration from 0 to x"""
    f= np.exp(x) - 1
    return f

def step(n):
    """defines the step size; using upper and lower integration limit"""
    upper = 1.
    lower = 0.
    f=(upper - lower)/n
    return f

def intRec(n):
    "n is the number of evaluation points, i.e. number of terms in the sum"
    k = np.arange(n+1)
    terms = np.exp(k*step(n))
    f = step(n)*np.sum(terms)
    return f

def intTrap(n):
    k = np.arange(1,n)
    terms = np.exp(k*step(n))
    f = step(n)*0.5*(1+np.exp(n*step(n)))+step(n)*np.sum(terms)
    "remember different weighting for first and last term"
    return f

# example computation    
print ("analytic: ", analytic(1),"\n", "rectangular: ", intRec(100),"\n", "trapezoid: ", intTrap(100), "\n", "evaluation using 100 terms in the sum")

# now let's graph the results to explore the dependency on number of evaluation points
x=np.arange(1,10000,100);
y1=np.array([intRec(q) for q in x])
z1=abs(y1-analytic(1));
z1/=analytic(1);

plt.figure(1)
plt.xlabel('log(n)')
plt.ylabel('log(error)')
plt.title('relative error for rectangular approximation')
plt.plot(np.log(x),np.log(z1))
plt.show()

y2=np.array([intTrap(q) for q in x])
z2=abs(y2-analytic(1));
z2/=analytic(1);

plt.figure(2)
plt.xlabel('log(n)')
plt.ylabel('log(error)')
plt.title('relative error for trapezoid approximation')
plt.plot(np.log(x),np.log(z2))
plt.show()

plt.figure(3)
plt.xlabel('log(n)')
plt.ylabel('log(error)')
plt.title('relative error for both approximations')
plt.plot(np.log(x),np.log(z1))
plt.plot(np.log(x),np.log(z2))
plt.show()
