# code for PY3C01-numerical methods
# Methods of ODE's, September 2014

# aim:
# to numerically solve an ODE of the form y' = f(x,y)

import numpy as np
import matplotlib.pyplot as plt

# define functional form of f(x,y)
def f(x,y):
	f = y
	return f

# define analytic solution (if known)
def analytic(x,y):
	f = np.exp(x)
	return f

# define simple euler update
def seuler(x,y,step):
	y_new = y + step*f(x,y)
	return y_new

# define modified euler update
def meuler(x,y,step):
	y_mid = y+step*0.5*f(x,y)
	y_new = y + step*f(x+0.5*step,y_mid)
	return y_new

# define improved euler update
def ieuler(x,y,step):
	y_new = y + 0.5*step*( f(x,y) + f(x+step, y + step*f(x, y)) )
	return y_new

# define Runge Kutta update
def rk(x,y,step):
	k1 = f(x,y)
	k2 = f(x + 0.5*step, y + 0.5*step*k1)
	k3 = f(x + 0.5*step, y + 0.5*step*k2)
	k4 = f(x + step, y + step*k3)
	y_new = y + step/6.0*(k1 + 2.0*k2 + 2.0*k3 + k4) 
	return y_new

# Set the initial values of x and y, define the step size
step = 0.05; start = 0.0; end = 1.0
y_zero = 1

# n is the number of steps
n = int((end-start)/step)

# pre-allocate arrays
x = np.arange(start,end,step)
seul = np.zeros(n)
meul = np.zeros(n)
ieul = np.zeros(n)
ruku = np.zeros(n)
real = np.zeros(n)

seul[0] = y_zero
meul[0] = y_zero
ieul[0] = y_zero
ruku[0] = y_zero
real[0] = y_zero

# increment solutions
for i in range(1,n):
	seul[i] = seuler(x[i-1], seul[i-1], step)
	meul[i] = meuler(x[i-1], meul[i-1], step)
	ieul[i] = ieuler(x[i-1], ieul[i-1], step)
	ruku[i] = rk(x[i-1], ruku[i-1], step)
	real[i] = analytic(x[i],real[i])

# plot solutions
plt.figure(1)
plt.xlabel('X')

plt.ylabel('Y')
#line_1,=plt.plot(x,real)	
#line_2,=plt.plot(x,seul)	
#line_3,=plt.plot(x,meul)	
#plt.plot(x,ieul)	
#plt.plot(x,ruku)	
#plt.legend([line_1,line_2],['analytic','simple Euler'],loc=2)
#plt.legend([line_1,line_2,line_3],['analytic','simple Euler', 'modified Euler'],loc=2)
#plt.title('integration of dy/dx = y')
#plt.show()

line_2,=plt.plot(x,abs(seul-real)/real)	
line_3,=plt.plot(x,abs(meul-real)/real)	
line_4,=plt.plot(x,abs(ieul-real)/real)	
#line_5,=plt.plot(x,abs(ruku-real)/real)	
#plt.legend([line_3,line_5],['modified Euler', 'Runge-Kutta'],loc=2)
#plt.legend([line_5],['Runge-Kutta'],loc=2)
plt.legend([line_2,line_3,line_4],['simple Euler','modified Euler', 'improved Euler'],loc=2)
#plt.legend([line_3,line_4,line_5],['modified Euler','improved Euler', 'Runge-Kutta'],loc=2)
#
#plt.legend([line_5],[ 'Runge-Kutta'],loc=2)
#plt.title('relative error')
plt.show()


