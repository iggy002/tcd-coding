# -*- coding: utf-8 -*-
"""
Created on Fri Nov 10 10:11:06 2017

@author: SHUTZLER
"""

# code for PY3C01-numerical methods # Random Numbers, September 2014

# aim:
# three correlation tests

import numpy as np
import numpy.random as ran
import matplotlib.pyplot as plt

# random numbers generator in numpy.random called by random()
# int may be seeded by seed(seed_val)
seed_val = 4
ran.seed(seed_val)
print ("seed:", seed_val, " random number: ", ran.random())

ran.seed(seed_val)
print ("first ten random numbers for seed ",  seed_val, ran.random(10))
print ("\n")

# 1. Plot of r_i+1 vs r_i

number = 20000
ran.seed(seed_val)
a = ran.random(number)

ran.seed(seed_val)
ran.random()
b = ran.random(number)

plt.figure(1)
plt.axes().set_aspect('equal')
plt.xlabel('r_i value')
plt.ylabel('r_i+1 value')
plt.plot(a,b,'.',markersize=1)
plt.show()

# 2. Evaluation of k-th moment

number = 10000
k = 4.
total = np.sum(ran.random(number)**k)
ans = 1./(1.+k)
print ("analytical result: ", ans)
print ("numerical result: ", total/number)
print ("relative difference: ", abs(total/number - ans)/ans)
print ("\n")

# 3. Computation of integral over r_i+1 r_i as a function of N

number = 10000
ran.seed(seed_val)
a = ran.random(number)

ran.seed(seed_val)
ran.random()
ran.random()
ran.random()

b = ran.random(number)

c = a*b
total = np.sum(c)
print ("is this sum close to a quarter? ",total/number)
print ("\n")

# check convergence to the value 0.25 for independent random numbers

conv1 = np.zeros(number)
conv2 = np.zeros(number)
for i in range(2,number):
	conv1[i] = conv1[i-1] + c[i] #partial sum up to i
	conv2[i] = conv1[i]/i #partial average up to i

plt.figure(2)
plt.xlabel('i') 
plt.ylabel('conv2[i]')
plt.plot(conv2,',')
plt.ylim([0.22,0.28])
plt.plot(np.ones(number)*0.25) #create an array of ones, multiplied by 0.25
plt.show()

# to see fluctuations, subtract 0.25 
# fluctuations should decay with square root of n 

conv2 -= 0.25
n = np.array([1/np.sqrt(i) for i in range(1,number+1)])

plt.figure(3)
plt.xlabel('i')
plt.ylabel('fluctuation')
plt.plot(conv2,',')
plt.plot(n)
plt.plot(-n)
plt.ylim([-0.05,0.05])
plt.show()

n = np.arange(1,number+1)
plt.figure(4)
plt.axes().set_aspect('equal')
x = n**-0.5
plt.plot(np.log(n),np.log(x))
plt.xlabel('log(i)')
plt.ylabel('log(modulus of fluctuation)')
plt.plot(np.log(n),np.log(np.abs(conv2)),',')
plt.show()
