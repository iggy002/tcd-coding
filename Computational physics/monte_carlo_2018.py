# code for PY3C01-numerical methods
# Monte Carlo Integration, November 2014

# aim:

import numpy as np
import numpy.random as rn
import matplotlib.pyplot as plt
from scipy.integrate import quad
import scipy.special as scp

# define function to be integrated
def f(x):
	f = np.exp(x)
	return f

# define the analytic solution (if known)
# in this case, the definitive integral of f(x) as a function of the limits
def analytic(a,b):
	f = np.exp(b) - np.exp(a)
	return f

def meansquare(a,b):
	f = 0.5*(np.exp(2*b) - np.exp(2*a))
	return f

# define modified function to be integrated
def fmod(x):
	f = np.exp(np.sqrt(1+2*x)-1)/np.sqrt(1+2*x)
	return f

# define the modified analytic solution (if known)
# in this case, the definitive integral of fm(x) as a function of the limits
def modanalytic(a,b):
	f = np.exp(np.sqrt(1+2*b)-1) - np.exp(np.sqrt(1+2*a)-1)
	return f

# this form comes from the analytical integration
def modmeansq(a,b):
	f = (scp.expi(2.0*np.sqrt(1+2*b)) - scp.expi(2.0*np.sqrt(1+2*a)))/np.exp(2)
	return f

# integrand
def fmodsq(x):
	f = fmod(x)*fmod(x)
	return f

# analytical mean and standard deviation
mean = analytic(0,1)
mean2 = meansquare(0,1) 
sdev = np.sqrt(mean2 - mean*mean)

print
print ("sample calculations for Monte Carlo integration, int_0^1 exp(x)=?")
print
print( "analytical result = %0.5f = e-1 "  % analytic(0,1))
#print( "mean2 = " mean2)
print( "analytical standard deviation of the function = sqrt(mean2 - mean*mean) = %0.3f" %sdev )

modmean = 2./3.*modanalytic(0,1.5)
modmean2 = 2./3.*modmeansq(0,1.5)
	# numerical integration
modmeanq = quad(fmodsq,0,1.5)
modsdev = np.sqrt(modmean2 - modmean*modmean)
mod_analytic = modanalytic(0,1.5)

#print( "analytical mean  of modified fn= " modmean
#print( "mean2 = " modmean2
#print( "quad mean2 = " modmeanq[0]*2/3
print( "analytical standard deviation of the modified function = sqrt(modmean2 - modmean*modmean) = %0.3f " %modsdev)

# number of points for monte carlo integration
mc_max = 100
# number of mc integrations for statistics
rep_max = 10000
seed = 4
estimate1 = np.zeros(rep_max)
estimate2 = np.zeros(rep_max)

for i in range(rep_max):
	for j in range(mc_max):
		estimate1[i] += f(rn.rand())
		estimate2[i] += fmod(1.5*rn.rand())
	estimate1[i] /= mc_max
	estimate2[i] /= mc_max
	estimate2[i] *= 1.5

mc_mean = np.mean(estimate1)
mc_mean_mod = np.mean(estimate2)
mc_sdev = np.std(estimate1)
mc_sdev_mod = np.std(estimate2)
rel_error = abs( (mean-mc_mean)/mean)
rel_error_mod = abs( (mod_analytic-mc_mean_mod)/mod_analytic)

print ("\n")
print("direct Monte Carlo integration, using %d random points for sampling the function" %mc_max)
print( "mc mean = %0.4f" % mc_mean)
#print( "relative error = %0.3g" % rel_error)
print( "mc standard deviation = %0.3f based on 10000 MC integrations" % mc_sdev)
print( "analytical standard deviation / sqrt(mc_max) = %0.3f" % (sdev/np.sqrt(mc_max)) )
print("(see graph) \n")

print("Monte Carlo integration of modified function, using %d random points for sampling the function" %mc_max)
print( "mc mean for modified fn = %0.4f" % mc_mean_mod)
#print( "relative error for modified fn = %0.3g" % rel_error_mod)
print( "mc standard deviation for modified fn = %0.3f based on 10000 MC integrations" % mc_sdev_mod)
print( "analytical standard deviation for modified fn / sqrt(mc_max) = %0.3f" % (modsdev/np.sqrt(mc_max)) )
print("(see graph) \n")
print ("importance sampling reduces error in MC calculation using the same number of Monte Carlo steps (and thus same runtime)!")

plt.figure(1)
plt.hist(estimate1)
plt.hist(estimate2)
plt.axvline(x=mean,color='r')
plt.show()

