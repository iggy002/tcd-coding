import numpy as np 
import matplotlib.pyplot as plt 


"""
# Assignment 1: Age of Discovery
"""
# 1. Load data + Manipulate/clean 
data = np.loadtxt('planets_2020.csv', delimiter=',', dtype=str  )

# Column names 
labels = data[0] # gets the first row and creats a o

# Missing values 
data[np.where(data=='')] = '0' # where ther is no value , replace with a 0 

# Creating columns 
disc_year = data[1:, 3].astype(int)
pl_name = data[1:, 1] # takes all rows from 2nd to last and the 2nd column
pl_mass = data[1:, 6].astype(float)
"""
a)
Find the number of planets discovered each year, and plot the number of planets discovered vs year.
(Hint: This can be done manually as in Exercise 2, or using a histogram tool such as np.hist() or plt.hist()
If using one of these tools, be careful about the definition of the bins!)
"""
unique_years = np.unique(disc_year)

a = []
for i in range(len(unique_years)):
    a.append(  np.where((disc_year == unique_years[i] ) ))
# data[disc_year == 2012 ] 

count = 0
for i in range (len(a)):
    print (len(a[i][0]))
    count = len(a[i][0]) + count 

new_dict = dict(zip(unique_years, a))
k=list(new_dict.keys())    

fig=plt.figure()
ax=fig.add_subplot(111)
ax.plot( disc_year[a[0]], pl_mass [a[0]], label='Min. Temp')
#ax.plot( disc_year[jan_1987], pl_mass [jan_1987], label='Max. Temp')

plt.legend(fontsize =12)
plt.show()
    
"""
b)
Find the smallest and largest planet mass (if known), and plot these as two curves against the year of discovery.
(Hints: the mass will be 0 if unknown;
look at the jan 1987 example in the numpy_files notebook if you get stuck...;
use .semilogy instead of .plot to make the y-axis scale logarithmic in your plot)
"""




#jan_1987= np.where((month==1) & (year==1987) )
"""
fig=plt.figure()
ax=fig.add_subplot(111)
ax.plot( disc_year[jan_1987], pl_mass [jan_1987], label='Min. Temp')
ax.plot( disc_year[jan_1987], pl_mass [jan_1987], label='Max. Temp')

"""  
    
    
    
    
    
    
    
# Fixing stuff 
first10 = disc_year.argsort()[0:10]

# print the year and the name of the planet ( first 10 )
#for i in first10:
#    print ('%-10i %-18s' % (disc_year[i], pl_name[i]) )

#sizes = np.unique(list, return_counts=True)
big_planets = ( pl_mass > 25).nonzero()

#np.unique(list, return_counts=True)
ordering = np.argsort(disc_year[big_planets])
### 2. Exersise 



#np.where((month == 12) &(day == 25))



unique_years = np.unique(disc_year)



pl_mass = data[1:, 6].astype(float) # repeat of above for convenience