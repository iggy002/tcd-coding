#!interpreter [optional-arg]
# -*- coding: utf-8 -*-


"""
# Exercise 1: The New Big Things
Find all planets with a mass AT LEAST 25 times the mass of Jupiter (the unit used in the database).
List the year of discovery, planet name and planet mass.
Sort the list chronologically.

#Exercise 2: System Sizes
Count the number of instances of each system size.
(e.g. How many stars have 1, 2, 3..... planets?)
Plot the number of instances against the system size.

# Assignment 1: Age of Discovery

a)
Find the number of planets discovered each year, and plot the number of planets discovered vs year.
(Hint: This can be done manually as in Exercise 2, or using a histogram tool such as np.hist() or plt.hist()
If using one of these tools, be careful about the definition of the bins!)

b)
Find the smallest and largest planet mass (if known), and plot these as two curves against the year of discovery.
(Hints: the mass will be 0 if unknown;
look at the jan 1987 example in the numpy_files notebook if you get stuck...;
use .semilogy instead of .plot to make the y-axis scale logarithmic in your plot)


# Exercise 3: Let it snow
Print the 10 hottest and coldest Christmas Days since 1942

# Assignment 2: Irish Summers
a) 
Plot total summer rainfall versus year, and print the years with the driest and wettest summers
(defining June 1st to August 31st as the summer period)
Hint: use | (or) instead of (and) to combine conditions for different months

b) 
Find the single day in the database with the largest fluctuation in temperature.
Plot the temperature fluctuation on this date as a function of year.


# Assignment 3: Big Cities 

Use numpy tools to obtain the following from the cities database:

a) A list of the 10 most populous cities in China (code CHN), and their populations

b) A list of the 5 countries with the greatest number of cities in the database.

c) A histogram, showing the breakdown of city sizes, for the country with the most cities.  
_Hint: For the best results here, you may need to manually change the binsizes and/or axes scales to logarithmic._
"""
# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
from functools import wraps
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED
# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'



# This is for learning logging 

def logg(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f'just ran step {func.__name__} shape={result.shape} took {time_taken}s')
        return result
    return wrapper

# Method 1 : Pandas #

### Functions ( for pipelines )
@logg
def start_pipleine(dataframe):
    #returns a copy , as we dont want to change to origanal 
    return dataframe.copy()

@logg
def planets_base_dataframe(data):
    data = data[16:]                         # gets rid of unwanted rows 
    base = pd.DataFrame(data )               # crates dataframe 
    base = base.rename(columns=data.iloc[0]) # renames columns 
    base = base[1:]   
    base.replace(np.nan, 0, inplace=True)                  # replace nan values with 0   
          
    return base

@logg
def correct_dtypes_planets_base_dataframe(base):
    #setting data types 
    base = base.astype({'hostname': str,
                        'pl_name': str,
                        'sy_pnum': int,
                        'disc_year': int,
                        'pl_orbper': float,
                        'pl_orbsmax': float,
                        'pl_bmassj': float,
                        'st_teff': float, 
                        'st_rad': float,
                        'st_mass': float,
                        'sy_dist': float ,
                        })    
    
    return base     
   
@logg 
def the_new_big_thing(base):
    base = base.loc[ base['pl_bmassj'] >= 25 ]  # drops all planets that don't fit this criteria for mass 
    base = base [['pl_name','disc_year', 'pl_bmassj']]
    base = base.sort_values(by=['disc_year'],ascending=False)
    
    return base 

@logg
def system_sizes(base):
    #unique_stars = base[['hostname']].unqiue
    number_of_times_each_star_comes_up = base['hostname'].value_counts().to_frame() 
    number_of_times_each_star_comes_up.reset_index(inplace=True) 
    number_of_times_each_star_comes_up.rename(columns = {'index': 'hostname', 'hostname': 'how many times each star name comes up'}, inplace = True)
    # x is a dataframe showing how many times each star name comes up 
    
    possible_system_sizse = base['sy_pnum'].value_counts().to_frame()
    possible_system_sizse.reset_index(inplace=True)
    possible_system_sizse = possible_system_sizse.sort_values(by=['index'],ascending=False) 
    
    #now one can use .plot() for the dataframe , but in this case a more detailed plot is needed 
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    data_dots = ax1.plot(possible_system_sizse['index'],possible_system_sizse['sy_pnum'] ,'o',  label='number of times system size occurs')
    data_line = ax1.plot(possible_system_sizse['index'],possible_system_sizse['sy_pnum'] )
    ax1.set_xlabel('size of system', fontsize = 14)
    ax1.set_ylabel(r'number of systems ', fontsize = 14)
    ax1.legend()
    plt.show()
    return possible_system_sizse

#@logg
def age_of_discovery(base):
    #the number of planets discovered each year
    number_of_planets_discovered_each_year = base.groupby(['disc_year'])['pl_name'].value_counts()
    number_of_planets_discovered_each_year = number_of_planets_discovered_each_year.groupby(['disc_year']).value_counts().to_frame() 
    number_of_planets_discovered_each_year.rename(columns = {'pl_name': 'Number of Planets Discovered'}, inplace = True)
    number_of_planets_discovered_each_year.reset_index(inplace=True)
    number_of_planets_discovered_each_year = number_of_planets_discovered_each_year.drop(['pl_name'], axis=1)
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    data_dots = ax1.plot(number_of_planets_discovered_each_year['disc_year'],number_of_planets_discovered_each_year['Number of Planets Discovered'] ,'o',  label='number of planets discovred')
    data_line = ax1.plot(number_of_planets_discovered_each_year['disc_year'],number_of_planets_discovered_each_year['Number of Planets Discovered'] ,  )
    ax1.set_xlabel('Year', fontsize = 14)
    ax1.set_ylabel(r'Number of Planets Discovered', fontsize = 14)
    ax1.legend()
    plt.show()
    
    #Find the smallest and largest planet mass
    mass = base[['pl_name','disc_year','pl_bmassj']]
    mass = mass.sort_values(by=['pl_bmassj'],ascending=True) 
    mass = mass.loc[ mass['pl_bmassj'] > 0 ] # drop zero values 
    mass_min = mass.groupby(['disc_year'])['pl_bmassj'].agg(np.min)
    mass_max = mass.groupby(['disc_year'])['pl_bmassj'].agg(np.max)
    
    # Plot mass size and the year for which the largest mass and smallest mass were discovered 
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    data_dots = ax2.semilogy(mass_min , )
    data_dots = ax2.semilogy(mass_min ,'o' , label='min mass for a given year ')
    data_line = ax2.semilogy(mass_max, )
    data_line = ax2.semilogy(mass_max,'o', label='max mass for a given year ' )
    ax2.set_xlabel('Year', fontsize = 14)
    ax2.set_ylabel(r'pl_bmassj', fontsize = 14)
    ax2.legend()
    plt.show()
    
    
    return mass_max ,number_of_planets_discovered_each_year , 

@logg
def correct_dtypes_weather_base_dataframe(base):
    #setting data types 
    base = base.astype({'# day': int,
                        ' month': int,
                        ' year': int,
                        ' min_temp (C)': float,
                        ' max_temp (C)': float,
                        ' rainfall (mm)': float,
                        ' windspeed (knot)': float,
                        })
    return base    

#@logg 
def let_it_snow(base):
    day = 25 # day that we want 
    month = 12 # month that we want 
   
    mask = ( base['# day'] == day) & (base[' month'] == month) #creat a list of true or false based on mask parameters 
    base = base.loc[mask] #makes a dataframe containing true only values 
    
    coldest = base.sort_values(by=[' min_temp (C)'],ascending=True) 
    coldest = coldest[:10]
    
    warmest = base.sort_values(by=[' max_temp (C)'],ascending= False) 
    warmest = warmest[:10] 
    
    print (coldest)
    print (warmest)
    return base , coldest ,warmest 

def irish_summers(base):
    
    base_b = base#.copy()
    # want 1st of june - 31st of august for each year 
    mask = ( base[' month'] > 5) & (base[' month'] < 9) #creat a list of true or false based on mask parameters 
    base = base.loc[mask] #makes a dataframe containing true only values 
    
    # want the summer rain fall ( assumed that thats the total rain for a given year )
    summer_rain = base.groupby([' year'])[' rainfall (mm)'].agg(np.sum).to_frame()
    summer_rain.reset_index(level=0, inplace=True)
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    # graph of total rain full per summer 
    
    data_dots = ax1.plot(summer_rain[' year'],summer_rain[' rainfall (mm)'] ,'o',  label='rail fall for a given summer')
    data_line = ax1.plot(summer_rain[' year'],summer_rain[' rainfall (mm)'] ,  )
    ax1.set_xlabel('Year', fontsize = 14)
    ax1.set_ylabel(r' rainfall (mm)', fontsize = 14)
    ax1.legend()
    plt.show()
    
    #print('The most rain for a give summer was %' % summer_rain.min())
    #print(summer_rain.max())
    
    #wettest_summer = summer_rain[' rainfall (mm)'].max()
    wettest_summer = summer_rain[summer_rain[' rainfall (mm)']==summer_rain[' rainfall (mm)'].max()]  
    t_1 = summer_rain.iloc[16]
    #print (wettest_summer )
    
    driest_summer = summer_rain[summer_rain[' rainfall (mm)']==summer_rain[' rainfall (mm)'].min()]  
    #t_1 = driest_summer.iloc[16]
    #print (wettest_summer )
    
    print("The wettest summer  '{summer_rainmax}' and  the driest summer  {summer_rainmin} unique categories".format(summer_rainmax =wettest_summer  , summer_rainmin =  driest_summer))
    

    
    # b
    base_b['diff'] =  base_b[' max_temp (C)'] - base_b[' min_temp (C)']
    answer =  base_b[base_b['diff']==base_b['diff'].max()] # largest fluctuation 
    answer_per_year =   base_b.groupby([' year',' month'])['diff'].agg(np.max).to_frame()
    answer_per_year.reset_index(level=0, inplace=True)
    answer_per_year.reset_index(level=0, inplace=True)
    war =  answer_per_year.groupby([' year'])['diff'].agg(np.max).to_frame()
    #answer_per_year.plot()
    #war.plot()
    
    # Last part of q2 (max fluctuation)
    last_hr = base_b[ (base_b['# day']  == 2 ) & (base_b[' month'] == 6)  ]
  
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    
    data_dots = ax2.plot(last_hr[' year'],last_hr['diff'] ,'o',  label='fluctuation (°C)')
    data_line = ax2.plot(last_hr[' year'],last_hr['diff']  ,  )
    ax2.set_xlabel('Year', fontsize = 14)
    ax2.set_ylabel(r'Temperature (°C)', fontsize = 14)
    ax2.legend()
    plt.show()
    
    return summer_rain , wettest_summer ,t_1 ,driest_summer  ,base_b , answer ,answer_per_year ,war ,last_hr



def big_cities_base(base)  :
    
    # filter by cities 
    countries = base[' country'].unique() 
    trick = base[' country'].unique()
    
    # dic , with keys being the name of each country, keys store dataframe with all the entries for that country 
    countries_dict = dict()
    
    
    # Get list of ID's for each 'Category'
    for i in range (len( trick)):
        countries[i] = pd.DataFrame(base.loc[base[' country'] == trick [i]] )
    
    for i in  range (len(trick)):
        n= trick[i]
        countries_dict[n] = countries [i]
        
    # (a)
    chines_cities = countries_dict['CHN']
    
    # (b)
    countries_ranked_number_of_cities = base[' country'].value_counts().to_frame() 
    countries_ranked_number_of_cities.reset_index(inplace=True) 
    countries_ranked_number_of_cities.rename(columns = {'index': ' country', ' country': 'Number of cities'}, inplace = True)
    
    top_5_number_of_cities = countries_ranked_number_of_cities [:5]
    
    # (c)
    usa = countries_dict['USA'].sort_values(by=[' population'],ascending= False).reset_index()
    
    fig1 = plt.figure(1000) #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    # graph of total rain full per summer 
    
    data_dots = ax1.semilogy(usa.index,usa[' population'] ,'o',  label='population per city')
    #data_line = ax1.plot(summer_rain[' year'],summer_rain[' rainfall (mm)'] ,  )
    ax1.set_xlabel('City', fontsize = 14)
    ax1.set_ylabel(r'Population', fontsize = 14)
    ax1.legend()
    plt.show()
    
    fig2 = plt.figure(1000) #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    # graph of total rain full per summer 
    
    #data_dots = ax2.hist(usa.index,usa[' population'] ,width=(bins[-1]-bins[-2]))
    #data_line = ax1.plot(summer_rain[' year'],summer_rain[' rainfall (mm)'] ,  )
    ax2.set_xlabel('City', fontsize = 14)
    ax2.set_ylabel(r'Population', fontsize = 14)
    ax2.legend()
    plt.show()
    
    
    
    return countries_dict ,chines_cities ,top_5_number_of_cities ,usa ,fig1



### main 

# Load files into dataframes
data_1 = pd.read_csv('planets_2020.csv')
data_2 = pd.read_csv('weather.csv')
data_3 = pd.read_csv('cities.csv')


# Chaining 
cleaned_data = (data_1
 .pipe(start_pipleine) # makes copy of dataframe 
 .pipe(planets_base_dataframe) # cleans dataframe so its useable 
 .pipe(correct_dtypes_planets_base_dataframe)
 )

# Answer for exersise 1 
Exersise_1 = (cleaned_data
 .pipe(start_pipleine)
 .pipe(the_new_big_thing)
 ) # the final dataframe object retunred is the answer 

# Answer for exersise 2
Exersise_2 = (cleaned_data
 .pipe(start_pipleine)
 .pipe(system_sizes)
 )

# Answer for Assignment_1 
Assignment_1 = (cleaned_data  #might need to check answer 
 .pipe(start_pipleine)
 .pipe(age_of_discovery)
 )

# Answer for exersise 3
Exersise_3 = (data_2
 .pipe(start_pipleine)
 .pipe(correct_dtypes_weather_base_dataframe)
 .pipe(let_it_snow)
 ) 

# Answer for Assignment_2 
Assignment_2 = (data_2
 .pipe(start_pipleine)
 .pipe(correct_dtypes_weather_base_dataframe)
 .pipe(irish_summers)
 ) 

# Answer for Assignment_3 
Assignment_3 = (data_3
 .pipe(start_pipleine)
 .pipe(big_cities_base)
) 

Assignment_3_a = Assignment_3[1][:10]

Assignment_3_b = Assignment_3[2]

Assignment_3_c = Assignment_3[4]


# This is for generating the histogram for the last question of Assignment_3
t = Assignment_3[3][[" population"]]
tt = t.hist (bins=np.logspace(np.log10(1),np.log10(2000000), 90))
plt.title('Breakdown of City Sizes')
for ax in tt.flatten():
    ax.set_xlabel("Population")
    ax.set_ylabel("Number of Cities")