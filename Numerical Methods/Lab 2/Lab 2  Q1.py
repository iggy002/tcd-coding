#!interpreter [optional-arg]
# -*- coding: utf-8 -*-


"""

# Assignment 1: The Mandelbrot set

Use the new function to create plots of the following 4 regions of the Mandlebrot set.
In each case use an appropriate number of iterations and grid points, and colour map, to produce a clear plot.
Produce a 2x2 subplot grid to present the results.

i)  −0.625<𝑥<−0.425  ;  −0.7<𝑦<−0.5 
ii)  −0.575<𝑥<−0.550  ;  −0.650<𝑦<−0.625 
iii) A circular shape, centered at (-1.0, 0.0) and with  𝑟=0.25 , is obvious in the zoom-out plots above.

Points on the edge of this circle are given by  (−1,0)+𝑟(cos𝜃,sin𝜃) .

Plot the Mandlebrot set in the square region of side length 0.02, which is centered at the edge point where  𝜃=(0.𝐴𝐵𝐶𝐷)×2𝜋 , where  𝐴𝐵𝐶𝐷  are the 2nd, 4th, 6th and 8th digits of your student number. (i.e. student number = 12345678 --> 0.ABCD = 0.2468 )

iv) A zoom of an interesting feature from iii)


"""

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq 
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED

# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))


# Note on logging 
# The @logg decorator was used when fine tunning the code , but is commented out currently for the final version . 
def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper




############################################


# function for producing the curves 

student_number =  0.7244

@logg_non_dataframe
def Mandelbrot_fun_vec(X, Y, maxruns):
    M = np.zeros_like(X)                       
    z = np.zeros_like(X, np.complex)
    
    for i in range(maxruns+1):
        tempvec = np.less( abs(z), 2.0)
        M[tempvec] = i - 1
        z[tempvec] = z[tempvec]*z[tempvec] + X[tempvec] + Y[tempvec]*1j
    return M


@logg_non_dataframe
def plots_i():
    """
    This was just for testing the code taken fromt he notebook solutions to the exerises 
    """
    dimptsa = 1000
    
    #plot parameters
    max_runs = [ 300,400  ]                     # maximum runs for each plot
    vnorms = []                                         # could use this to normalise vmax for each plot
    maxruns2=80
    x_mins = [-0.625,-0.575]
    x_maxs = [-0.425,-0.55]
    y_mins = [-0.7,-0.65]
    y_maxs = [-0.5,-0.625]
    for i in range( len( x_mins ) ):
        fig,ax =plt.subplots(figsize=(10,10))
        
        x1 = np.linspace( x_mins[i], x_maxs[i], dimptsa)
        y1= np.linspace( y_mins[i], y_maxs[i], dimptsa)
            
        X1,Y1 = np.meshgrid( x1, y1 )
        M1 = Mandelbrot_fun_vec(X1, Y1, max_runs[i])
        
        ax.pcolormesh( X1, Y1, M1, cmap=cm.PuOr, vmax=0.5*M1.max() )
        ax.set_xticks([])
        ax.set_yticks([])
        
        plt.tight_layout()
        plt.savefig('graph of ' +str(i) +'.png',dpi=800)

    
    return 








@logg_non_dataframe
def arrays_to_be_used():
    dimptsa = 2000

    #plot parameters
    max_runs = [ 400,400,400,400  ]                     # maximum runs for each plot
    vnorms = []                                         # could use this to normalise vmax for each plot
    
    x_mins = [-0.625,-0.575]
    x_maxs = [-0.425,-0.55]
    y_mins = [-0.7,-0.65]
    y_maxs = [-0.5,-0.625]
    
    
    r = 0.25
    theta = student_number * maths.pi
    centre = ( -1 + 0.25 * maths.cos( theta ), 0.25 * maths.sin( theta ) )
    print(centre)
    x_min_circle = centre[0] - 0.01
    x_max_circle = centre[0] + 0.01
    y_min_circle = centre[1] - 0.01
    y_max_circle = centre[1] + 0.01
    x_mins.append(x_min_circle)
    x_maxs.append(x_max_circle)
    y_mins.append(y_min_circle)
    y_maxs.append(y_max_circle)
    
    x_min_interesting = x_min_circle #+ ( 0.02 / 5.0 )
    x_max_interesting = x_max_circle - ( 3 * 0.02 / 5.0 )
    y_min_interesting = y_min_circle + ( 2 * 0.02 / 3.0 )
    y_max_interesting = y_max_circle #+ ( 3 * 0.02 / 5.0 )
    x_mins.append(x_min_interesting)
    x_maxs.append(x_max_interesting)
    y_mins.append(y_min_interesting)
    y_maxs.append(y_max_interesting)
    
    interesting_centre = ( ( (x_max_interesting - x_min_interesting) / 2.0 ) + x_min_interesting, ( (y_max_interesting - y_min_interesting) / 2.0 ) + y_min_interesting )
    print(interesting_centre)
    
    return x_mins, x_maxs, y_mins, y_maxs

@logg_non_dataframe
def plot_2x2():
    dimptsa = 1000 # I used 2000 for my actual plots 
    max_runs = [ 400,400,400,400  ] 
    
    fig = plt.figure(figsize=(13,13))
    plt.axis('off')
    #plt.title('Plot of Regions of The Mandelbrot Set (2x2 subplot grid)', fontsize = 'small')
    for i in range( len( x_mins ) ):
        print (i)
        ax = fig.add_subplot( 2, 2, i+1 )
        
        x1 = np.linspace( x_mins[i], x_maxs[i], dimptsa)
        y1= np.linspace( y_mins[i], y_maxs[i], dimptsa)
            
        X1,Y1 = np.meshgrid( x1, y1 )
        M1 = Mandelbrot_fun_vec(X1, Y1, max_runs[i])
        
        ax.pcolormesh( X1, Y1, M1, cmap=cm.PuOr, vmax=0.5*M1.max() )
        ax.set_xticks([])
        ax.set_yticks([])
    
    
    plt.savefig('graph_of_all_4 views .png',dpi=800)    
    plt.tight_layout()
    plt.show()


# Creating the array of the values to plot 
x_mins,x_maxs,y_mins,y_maxs = arrays_to_be_used()

plots_i()  # this isnt answering any of the questions 
plot_2x2()

  
   
    
