#!interpreter [optional-arg]
# -*- coding: utf-8 -*-


"""

# Assignment 2: The Wien displacement law
i) Use the function defined above to generate a longer set of  𝑇  and corresponding  𝜆𝑚𝑎𝑥  values in the same range.

ii) Use least square fitting methods in scipy to estimate the Wien displacement constant  𝑏 .

iii) Plot the fit, and compare the esimated and true values of  𝑏 .

"""

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
from functools import wraps

from scipy import constants
from scipy.optimize import minimize_scalar, leastsq
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED

# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))


# Note on logging 
# The @logg decorator was used when fine tunning the code , but is commented out currently for the final version . 
def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper




############################################


# function for producing the curves 
#@logg
def planck(wl, temp):
    return (2 * h * c**2 / wl**5) / (np.exp(h*c/(wl * kB * temp)) -1)   


# to find the max we find the minimum of the following function 
#@logg
def planck2_find_max(wl, temp):
    return -planck(wl, temp)

# finds the max for a given temp 
#@logg
def find_max_wl(temp):
    fmin=minimize_scalar(planck2_find_max, bracket=(0.2E-7, 2E-6), args=(temp))
    return fmin.x, -fmin.fun
@logg_non_dataframe
def maxl_maxv_arrays():
    ##positions of maxima for each T
    maxl, maxv = np.zeros_like(Ts), np.zeros_like(Ts)

    for i, temp in enumerate(Ts):
        maxl[i], maxv[i] = find_max_wl(temp)
    
    return maxl, maxv 

@logg_non_dataframe
def plot_maxl_vs_maxv(maxl,maxv):
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 

   
    
    points = ax1.plot(maxl,maxv,label = '$\lambda_{peak}$' )
    ax1.set_ylabel('Spectral Radiance $(kJ)$')
    ax1.set_xlabel('Wavelength $(nm)$')
    plt.legend(loc='upper right')
    plt.savefig('maxl_vs_maxv.png',dpi=200)
    #plt.show()

# function being used as the guess model 
def fun(Ts,B): # x = wls 
    return B*Ts # x = wls
   
#define the residuals function (difference between model and data)
def residuals(B, wl,Ts):
    B = B
    return wl - fun(Ts,B)

@logg_non_dataframe
def leastsq_fit_approach():
    
    fitting = leastsq(residuals,B ,args = (1/Ts,maxl)) 
   
    return fitting

@logg_non_dataframe
def graph_of_all_maximums():
    # good graph off all the maximums 

    fig, ax = plt.subplots()
    for i, temp in enumerate(Ts):
        line, = ax.plot(wls, planck(wls, temp)) #label=r'$T='+str(int(temp))+'\,\mathrm{K}$'
        dot, = ax.plot(maxl[i], maxv[i], 'o', color=line.get_color()) #sets the colour of the dot to that of the line 
    ax.ticklabel_format(axis='both', style='sci', scilimits=(-3,3))
    ax.set_ylabel('Spectral Radiance $(kJ)$')
    ax.set_xlabel('Wavelength (nm)')
    plt.title('$2000$ Temperatures Between $3000K - 8000K$', fontsize = 'small')
    plt.plot(maxl,maxv,'b',markersize=.02,label = '$\lambda_{peak}$')
    plt.legend(loc='upper right')
    plt.savefig('graph_of_all_maximums .png',dpi=800)
    #plt.show()
    
@logg_non_dataframe
def graph_of_some_maximums(start,stop,step,name):
    bb = maxl[start:stop:step]
    aa = maxv[start:stop:step]
    TT = Ts[start:stop:step]
    
    start_temp = start + 3000
    end_temp = 8000
    number_of_temps = round(stop/step)
    
    title = str(number_of_temps) + ' Temperatures Between:' + str(start_temp) + '$K$-' + str(end_temp) + '$K$ (step =' + str(step) + ')'
    
    fig, ax = plt.subplots()
    for i, temp in enumerate(TT):
        line, = ax.plot(wls, planck(wls, temp),markersize= 1) #label=r'$T='+str(int(temp))+'\,\mathrm{K}$'
        dot, = ax.plot(bb[i], aa[i], 'o', color=line.get_color(), markersize= 5) #sets the colour of the dot to that of the line 
    ax.ticklabel_format(axis='both', style='sci', scilimits=(-3,3))
    ax.set_ylabel('Spectral Radiance $(kJ)$')
    ax.set_xlabel('Wavelength $(nm)$')
    plt.title(title, fontsize = 'small')
    plt.plot(maxl,maxv,'b',markersize=.02,label = '$\lambda_{peak}$')
    plt.legend(loc='upper right')
    plt.savefig( str(name)+'.png',dpi=800)
    #plt.show()
    
@logg_non_dataframe
def graph_of_fit(maxl,y_fitted) :
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 

   
    
    points = ax1.plot(maxl, 1/y_fitted, '--', linewidth=3, label='fit')
    ax1.set_xlabel('$\lambda_{max} (nm)$')
    ax1.set_ylabel('$1/Temp (K)$')
    plt.title('Inverse Temperature against Maximum Wavelength' , fontsize = 'small')
    plt.legend(loc='upper right')
    plt.savefig('graph_of_fit.png',dpi=800)
    #plt.show()
  
# Define constants needed 
h = constants.Planck
c = constants.speed_of_light
kB = constants.Boltzmann

# arrays to feed into the function to produce the curves 
wls = np.linspace(1E-8, 1.2E-6, 2000) # The wave lenghts being considered 
Ts = np.linspace(3000, 8000, 2000)    # The temperatures being considered 

# Inital guess for the function being used to guess the model
B = .00001 

# need to fit this curve 
maxl,maxv =  maxl_maxv_arrays() # Creates two instances of the np.array . maxl: contains the lambda max
plot_maxl_vs_maxv(maxl,maxv)

 
# create an object that cotains the result of the fir
fitting = leastsq_fit_approach()

# creates an array of y values using the value of the coeffient from the leastsq_fit_approach()
y_fitted= fun(Ts, fitting[0][0] )

# Making a nice table for pdf
list_of_arrays = [maxl,maxv,Ts,wls]
table_of_values  = pd.DataFrame(np.stack(list_of_arrays).T).rename(columns = {0: 'Max Wavelength', 1: 'Max Spectral Radiance (kJ)' , 2: 'Temperature (K)' , 3: 'Wavelength'} )

print ("\nTable of Values")
print(table_of_values)
    
def main(): 
    
    # Took me a while to notice that I needed to take the recoprical 
    print("\nEstimated coefficient (befor taking the reciprocal) = {}".format(fitting[0]))
    print ("Estimated coefficient (after) = " +str(1/fitting[0][0])+ '\n')
    #plt.plot(Ts,fitting[0][0]/Ts)
    #plt.plot(Ts,constants.Wien/Ts,'o')

    
    # Graph the fit
    graph_of_fit(maxl,y_fitted) 
    
    # Graphs of the maximum 
    graph_of_all_maximums()
    graph_of_some_maximums(0,1999,20,1)
    graph_of_some_maximums(0,1000,20,2)
    graph_of_some_maximums(0,1999,100,3)
    graph_of_some_maximums(0,1999,150,4)
    
    # Creating objects to store the values of the Wien displacement constant. 
    # Calculating relative error and absolute error 
    Wien_displacement_constant_est = 1/fitting[0][0]
    Wien_displacement_constant_exact = constants.Wien
    Wien_displacement_constant_est_relative_error =  abs( (Wien_displacement_constant_est-Wien_displacement_constant_exact)/Wien_displacement_constant_exact )
    Wien_displacement_constant_est_absolute_error =  abs( (Wien_displacement_constant_est-Wien_displacement_constant_exact))

    
    print("\nEstimated coefficients Vs True Value : Using scipy \n \nWien_displacement_constant_exact = {} \nWien_displacement_constant_est = {}   \nWien_displacement_constant_est_relative_error   = {}    \nWien_displacement_constant_est_absolute_error   = {}"
          .format( Wien_displacement_constant_exact,Wien_displacement_constant_est, Wien_displacement_constant_est_relative_error, Wien_displacement_constant_est_absolute_error  )) 
  


  
if __name__ == "__main__": 
    main()     
    

