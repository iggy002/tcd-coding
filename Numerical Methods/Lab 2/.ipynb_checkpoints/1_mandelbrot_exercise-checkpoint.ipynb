{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from matplotlib import cm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  1. The Mandelbrot set\n",
    "\n",
    "In this exercise, we look at the Mandelbrot set -- a famous fractal.  \n",
    "The set is defined as the set of complex numbers c which for which repeated application of the map\n",
    "$$z_{n+1} = z_n^2 + c      \\qquad \\qquad \\mathrm{where} \\quad z_0=0$$\n",
    " does not produce a divergent sequence.\n",
    "\n",
    "A typical sequence starts $0,\\, c,\\, c^2+c,\\dots$    \n",
    "e.g.   \n",
    "$c = 1$ gives 0, 1, 2, 5, ....    which tends to infinity, so 1 IS NOT a member of the set.   \n",
    "$c=-1$ returns 0, -1, 0, -1, .... which is bounded, and so -1 IS a member of the set.\n",
    "\n",
    "The following cells calculate and plot the sequence for a given value of c"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# quickly print the first few elements of a sequence for a given c\n",
    "c = 0.33+0.5j\n",
    "c =np.complex(c)\n",
    "\n",
    "iterations = 5\n",
    "sequence=[0]\n",
    "\n",
    "i=0\n",
    "while (i < iterations):\n",
    "    next_element = sequence[-1]**2 + c\n",
    "    sequence.append(next_element)\n",
    "    i = i+1\n",
    "    \n",
    "print(sequence)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#plot the points generated above\n",
    "fig, ax = plt.subplots()\n",
    "\n",
    "#real and imaginary parts\n",
    "ax.set_title('Sequence with c = %.2f %+.2fi' %(c.real, c.imag))\n",
    "ax.plot(  np.real(sequence), np.imag(sequence), 'o:')\n",
    "ax.text(np.real(sequence[0]), np.imag(sequence[0])-0.25, 'initial' )\n",
    "\n",
    "ax.set_aspect('equal')\n",
    "ax.set_xlim(-2.5, 2.5)\n",
    "ax.set_xlabel('Re(z) ')\n",
    "ax.set_ylim(-2.5, 2.5)\n",
    "ax.set_ylabel('Im (z)')\n",
    "\n",
    "\n",
    "## threshold circle \n",
    "# thetas = np.linspace(0, 2*np.pi, 101)\n",
    "# ax.plot( 2*np.cos(thetas), 2*np.sin(thetas))\n",
    "\n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualise the Mandelbrot set, we normally calculate how many iterations $n$ it takes for $|z_n|$ to cross a certain, arbitrary threshold, usually $|z_n| \\ge 2$    \n",
    "(uncomment the relevant lines in the cell above to see this threshold).  \n",
    "We assume a point $c$ belongs to the set if we reach a high number of iterations without reaching the threshold.\n",
    "\n",
    "The following cells show a simple implementation which defines a function to return $n$ for a single value of $c$, and then loop over the points in a grid to create a plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Simple implementation\n",
    "\n",
    "#returns the value of the last 'n' for which |z_n| < 2, or maxruns if this does not occur\n",
    "def func(c, maxruns):\n",
    "    z = c            #first run\n",
    "    for i in range(maxruns):\n",
    "        if(abs(z)> 2):\n",
    "            return i\n",
    "        z = z**2 + c\n",
    "    return maxruns"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# calculate n over a grid of points c = x + yi\n",
    "xmin, xmax = -2.0, 0.5\n",
    "ymin, ymax = -1.25, 1.25\n",
    "dimpts = 100\n",
    "maxruns = 80\n",
    "\n",
    "x=np.linspace(xmin, xmax, dimpts)\n",
    "y=np.linspace(ymin, ymax, dimpts)\n",
    "\n",
    "X,Y = np.meshgrid(x, y)\n",
    "M = [func(xa+ya*1j, maxruns) for ya in y for xa in x]\n",
    "M = np.array(M).reshape(X.shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig,ax =plt.subplots(figsize=(6,6))\n",
    "ax.pcolormesh(X, Y, M, cmap=cm.PuOr, vmax=1.0*M.max())\n",
    "ax.set_xlabel('Re (c) ')\n",
    "ax.set_ylabel('Im (c)')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vectorisation\n",
    "\n",
    "In the above example, we used two nested for loops to perform the calculation at every grid point\n",
    "\n",
    "        M = [func(xa+ya*1j, maxruns) for ya in y for xa in x]\n",
    "        \n",
    "This is far from optimal -- ideally we want to take advantage of array methods within numpy to perform this calculation more efficiently.\n",
    "\n",
    "This is easy when we want to perform, e.g. multiplication, due to straightforward elementwise operations.\n",
    "\n",
    "It is more tricky in situations like this, when we want to find the the number of steps required for a particular condition to be true.\n",
    "\n",
    "To do so, we need to construct a new function, and can use numpy functions np.less or np.greater.\n",
    "\n",
    "Let's first look at how these functions work:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "temparray = np.linspace(0.0, 10.0, 11)\n",
    "lesscheck = np.less(temparray, 4.0)\n",
    "print(lesscheck)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This returns a Boolean array corresponding to evaluating \"is less than\" for each element in the initial array.\n",
    "\n",
    "This array can also be used to assign values"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "temparray2=np.zeros_like(temparray)\n",
    "print(temparray)\n",
    "temparray2[lesscheck] = 3.0\n",
    "print(temparray2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the value is only assigned if the corresponding element of lesscheck is 'True'\n",
    "\n",
    "<br>\n",
    "\n",
    "## Exercise 1:\n",
    "Write a vectorised version of func to calculate $n$ for all grid points together.\n",
    "\n",
    "Basic function layout:\n",
    "\n",
    "    def func_vec(X, Y, maxruns):\n",
    "        M = np.zeros_like(X)                       # output array \n",
    "        z = np.zeros_like(X, dtype=complex)        # values of z at each stage, here z0=0 for all initial points\n",
    "        \n",
    "        ##loop -- for each iteration:\n",
    "            ## find which indices correspond to z points still inside the threshold at this stage\n",
    "            ## write the loop index to the corresponding elements of M\n",
    "            ## update z (only need to do this for points still inside the threshold)\n",
    "        #return the output vector\n",
    "        \n",
    "Note there are several ways of writing this algorithm - this version updates the M vector for each point where |z| is LESS than the threshold at each step.\n",
    "\n",
    "This way, its last update (and outputted value) corresponds to the required value of $n$\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def func_vec(X, Y, maxruns):\n",
    "    M = np.zeros_like(X)                       \n",
    "    z = np.zeros_like(X, np.complex)\n",
    "    \n",
    "    ##loop over iterations goes here!\n",
    "\n",
    "\n",
    "    return M"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now check your new function by reproducing the figure above.\n",
    "\n",
    "Use %timeit to compare the speed of the func and func_vec routines for the lattice"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dimpts2=100\n",
    "maxruns2=80\n",
    "xn=np.linspace(xmin, xmax, dimpts2)\n",
    "yn=np.linspace(ymin, ymax, dimpts2)\n",
    "\n",
    "Xn,Yn = np.meshgrid(xn, yn)\n",
    "Ma = func_vec(Xn, Yn, maxruns2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from matplotlib import colors\n",
    "fig,ax =plt.subplots(figsize=(6,6))\n",
    "\n",
    "ax.pcolormesh( #########   )\n",
    "\n",
    "ax.set_xticks([])\n",
    "ax.set_yticks([])\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%timeit ##looping version"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%timeit ##vectorised version"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The new func_vec algorithm allows you to produce much higher definition plots, and run for larger numbers of iterations, which are sometimes required when calculating different sections of the set.\n",
    "\n",
    "## Homework Q1:\n",
    "Use the new function to create plots of the following 4 regions of the Mandlebrot set.   \n",
    "In each case use an appropriate number of iterations and grid points, and colour map, to produce a clear plot.   \n",
    "Produce a 2x2 subplot grid to present the results.\n",
    "\n",
    "\n",
    "i) $-0.625 < x < -0.425 $  ;   $ -0.7 < y < -0.5$\n",
    "\n",
    "ii) $-0.575 < x < -0.550 $  ;   $ -0.650 < y < -0.625$\n",
    "\n",
    "iii) A circular shape, centered at (-1.0, 0.0) and with $r=0.25$, is obvious in the zoom-out plots above.\n",
    "\n",
    "Points on the edge of this circle are given by $(-1,0) + r (\\cos \\theta, \\sin \\theta)$.\n",
    "\n",
    "Plot the Mandlebrot set in the square region of side length 0.02, which is centered at the edge point where $\\theta = (0.ABCD) \\,\\times  2\\pi$, where $ABCD$ are the 2nd, 4th, 6th and 8th digits of your student number.\n",
    "(i.e. student number = 12345678 --> 0.ABCD = 0.2468 )\n",
    "\n",
    "iv) A zoom of an interesting feature from iii)\n",
    "\n",
    "_Hint: Test these sets individually in spare cells below, and make a list of the parameters required to make a nice plot for each._   \n",
    "_You should see what effect varying maxruns, or vmin/vmax have on the resulting graphs._   \n",
    "_Create the composite figure by looping over 4-element lists containing the parameters for each plot_\n",
    "\n",
    "<br>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "stu_num = 0.2468\n",
    "dimptsa=300\n",
    "\n",
    "#plot parameters\n",
    "maxrunsa=[ 100,200,300,400  ]                     # maximum runs for each plot\n",
    "vnorms=[]                                         # could use this to normalise vmax for each plot\n",
    "\n",
    "xmins=[]\n",
    "xmaxs=[]\n",
    "ymins=[]\n",
    "ymaxs=[]\n",
    "\n",
    "fig = plt.figure(figsize=(10,10))\n",
    "\n",
    "for graph in range(4):\n",
    "    ax = fig.add_subplot(#, #, graph+1)\n",
    "    \n",
    "    x1=np.linspace(xmins[graph], xmaxs[graph], dimptsa)\n",
    "    y1= ######\n",
    "        \n",
    "    X1,Y1 = np.meshgrid( #####1)\n",
    "    M1 = ########\n",
    "    \n",
    "    ax.pcolormesh(######### )\n",
    "    ax.set_xticks([])\n",
    "    ax.set_yticks([])\n",
    "    \n",
    "\n",
    "    \n",
    "plt.tight_layout()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
