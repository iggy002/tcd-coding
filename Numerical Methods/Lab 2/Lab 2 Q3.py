#!interpreter [optional-arg]
# -*- coding: utf-8 -*-


"""

# Assignment 3: Planetary orbits

a) Generalise the function above to allow for an arbitrary number of objects, without any assumptions about their relative masses.
Assume the objects are all in-plane -- only x and y components vary.

To produce nice orbital plots for the systems below, work in the centre-of-mass frame.
i.e. correct the initial positions and velocities so that the centre-of-mass remains at the origin
Create a function to do this for any set of initial positions and velocities.


b) Consider a three-body system consisting of the Sun, Earth and Mars.
You can assume that Mars has a circular orbit, with radius  1.53  AU and mass  3.2×10−7𝑀𝑆 .
(You can also assume that Earth and Mars are aligned along the positive x-axis at  𝑡=0 )

Plot the following over a suitable time range:
-- the trajectories of all three bodies in the centre of mass frame.
-- the trajectory of the sun only, and explain the observed behaviour.
-- the separation between Earth and Mars as a function of time.


c) Horseshoe orbits can occur when a smaller body (such as an asteroid) and a large object (such as Earth) are in similar orbits around a star.
The asteroid's trajectory around the sun is affected by the gravitational pull of Earth when the two bodies are nearest in their orbits.
If the asteroid is on a lower, faster orbit than Earth, then the resulting force towards Earth shifts the asteroid to a higher, slower orbit, and vice versa.
A horseshoe orbit occurs when this shifts the asteroid from a lower to a higher orbit than Earth, so that it goes through a cycle of catching up and then falling behind the Earth.
The change in the shape of the asteroid's orbit are very small, but result in a horseshoe shape when mapped relative to both the Sun and Earth.

To model this:   
-- replace Mars with a body MUCH less massive than Earth, with an initial position directly opposite Earth.  
-- choose initial conditions so that this object is on a circular orbit with the same angular direction as Earth, but with a slightly smaller or greater radius.  
-- plot trajectories in a reference frame which rotates _with_ Earth so that a horseshoe-shaped orbit is clear


"""

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
from functools import wraps
from matplotlib import cm, colors

from scipy import constants
from scipy.optimize import minimize_scalar, leastsq
from scipy.integrate import odeint
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED
# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))


# Note on logging 
# The @logg decorator was used when fine tunning the code , but is commented out currently for the final version . 

def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper


############################################

# need to make function that doesnt account for the mass of the object 

# y contains (x1, y1, vx1, vy1)
def dydt_original(y, t, m2):
    output = np.zeros_like(y)
    output[0]=y[2]
    output[1]=y[3]
    output[2]= - 4*np.pi**2 *m2 * y[0] / (np.sqrt(y[0]**2 + y[1]**2))**3
    output[3]= - 4*np.pi**2 *m2 * y[1] / (np.sqrt(y[0]**2 + y[1]**2))**3
    return output   

# x,y components of force on body 1 by body 2
def x_force( x1, x2, y1, y2, m1, m2 ):
    G = 4*np.pi**2
    d = np.sqrt( (x1-x2)**2+(y1-y2)**2 )
    return -G*m1*m2*(x1-x2) / d**3

def y_force( x1, x2, y1, y2, m1, m2 ):
    G = 4*np.pi**2
    d = np.sqrt( (x1-x2)**2+(y1-y2)**2 )
    return -G*m1*m2*(y1-y2) / d**3

def dydt_gen( y, t, m, n ):
    output = np.zeros(4*n)
    
    for i in range(n):
        output[i] = y[i+2*n]
        output[i+n] = y[i+3*n]
        for j in set(range(n))-set([i]):
            output[i+2*n] += x_force( y[i], y[j], y[i+n], y[j+n], m[i], m[j] ) / m[i]
            output[i+3*n] += y_force( y[i], y[j], y[i+n], y[j+n], m[i], m[j] ) / m[i]
        
    return output         

#Corrects initial conditions so that centre-of-mass is fixed and at the origin
@logg_non_dataframe
def correct_for_cm( x0, y0, vx0, vy0, m ):
    num = len(x0)
    
    for i in range(num):  
        cmx = sum(x0*m) / sum(m)
        cmy = sum(y0*m) / sum(m)
        cm_vx = sum(vx0*m) / sum(m)
        cm_vy = sum(vy0*m) / sum(m)
        
    x0 = x0 - cmx
    y0 = y0 - cmy
    vx0 = vx0 - cm_vx
    vy0 = vy0 - cm_vy
    
    return x0, y0, vx0, vy0


@logg_non_dataframe
def plot_b():

    print('\n1st Plot')
    
    num = 3
    masses = np.array([ 1, 3e-6, 3.2e-7 ])
    x0 = np.array([ 0, 1, 1.53 ])
    y0 = np.array([ 0, 0, 0 ])
    vx0 = np.array([ 0, 0, 0 ])
    vy0 = np.array([ 0, 2*np.pi, 2*np.pi ])
    
    x0c, y0c, vx0c, vy0c = correct_for_cm( x0, y0, vx0, vy0, masses )
    input0 = np.concatenate( (x0c, y0c, vx0c, vy0c) )
    
    t = np.linspace( 0, 20, 8000 )
    data_to_plot = odeint( dydt_gen, input0, t, args=(masses,num)).T 
    
    fig, ax = plt.subplots( figsize=(6,6) )
    
    labels = ['Sun', 'Earth', 'Mars']
    colours = [ 'Orange', 'blue', 'red']
    widths = [12, 1, 1]
    for i in range(num):
        ax.plot( data_to_plot[i], data_to_plot[i+num], label=labels[i], color=colours[i], linewidth=widths[i] )
    
	
    	
    ax.legend( bbox_to_anchor=(1, 1.0) )
    
    ax.set_xlabel( '$X$ (AU)' )
    ax.set_ylabel( '$Y$ (AU)' )
    ax.set_title( 'Trajectories of 3 Bodies' )
    plt.savefig('Trajectories of 3 Bodies.png',dpi=400)
    
    print('\n2nd Plot')
    
    fig, ax = plt.subplots( figsize=(8,8) )
    ax.plot( data_to_plot[0], data_to_plot[num], 'y-', label='Sun Trajectory')
    ax.plot (0, 0, 'ko')
    ax.legend()
    
    ax.set_xlabel( '$X$ (AU)' )
    ax.set_ylabel( '$Y$ (AU)' )
    ax.set_title( 'Trajectory of Sun Around the CoM of the Solar System' )
    plt.savefig('Trajectories of Sun.png',dpi=400)
    
    print('\n3rd Plot')
    
     
    separation = np.sqrt( (data_to_plot[1]-data_to_plot[2])**2+(data_to_plot[1+num]-data_to_plot[2+num])**2 )
    
    fig, ax = plt.subplots( figsize=(10,10) )
    ax.plot( t, separation )
    ax.set_aspect( 'equal' )
    ax.set_xlabel( 'Time (years)' )
    ax.set_ylabel( 'Separation (AU)' )
    ax.set_title( 'Separation Between Earth and Mars as a Function of Time' )
    plt.savefig('Separation Between Earth and Mars as a Function of Time.png',dpi=400)



def plot_c():
    
    return 




plot_b()


# This is my attmept at c 
masses = np.array([ 1, 3e-6, 3.2e-7 ])


num=3
pl_mass= np.array([ 1, 3e-6, 3.2e-10 ])   #mass of planet
x0 = np.array([ 0, 1, -0.9 ])
y0 = np.array([ 0, 0, 0 ])

vx0 = np.array([ 0, 0, 0 ])
vy0 = np.array([ 0, 2*np.pi, 2*np.pi ])

x0c, y0c, vx0c, vy0c = correct_for_cm( x0, y0, vx0, vy0, masses )
input0 = np.concatenate( (x0c, y0c, vx0c, vy0c) )

t = np.linspace( 0, 20, 2000 )
data_to_plot = odeint( dydt_gen, input0, t, args=(masses,num)).T 

fig, ax = plt.subplots( figsize=(6,6) )

labels = ['Sun', 'Earth', 'Mars']
colours = [ 'orange', 'blue', 'red']
widths = [12, 1, 1]
for i in range(num):
    ax.plot( data_to_plot[i], data_to_plot[i+num], label=labels[i], color=colours[i], linewidth=widths[i] )
    
	
	
ax.legend( bbox_to_anchor=(1, 1.0) )


ax.set_xlabel( '$X$ (AU)' )
ax.set_ylabel( '$Y$ (AU)' )
plt.savefig('Last fig q3 orbits .png',dpi=400)


# was going to use this code from lorzen to on the sun plot 
"""

fig=plt.figure(figsize=(15, 6))

starting_point= 3500

for i, rho in enumerate(rho_range):
    ax = fig.add_subplot(2, 4, i+1, projection='3d')
    ax.axis('off')
    ax.grid(False)
    ax.set_title( r'$\rho = %.1lf $' % (rho_range[i]) , fontsize=16)
    ax.plot( statec[i,0,:starting_point], statec[i,1,:starting_point], statec[i,2,:starting_point], '--', alpha=0.5 )
    ax.plot( statec[i,0, starting_point:], statec[i,1,starting_point:], statec[i,2,starting_point:], c='b' )
    
    ax.set_xlim( statec[i,0, starting_point:].min(), statec[i,0, starting_point:].max())
    ax.set_ylim( statec[i,1, starting_point:].min(), statec[i,1, starting_point:].max())
    ax.set_zlim( statec[i,2, starting_point:].min(), statec[i,2, starting_point:].max())

    
    
plt.tight_layout()
plt.show()    
"""

