# -*- coding: utf-8 -*-
"""
# Section 1.4 : Numerical Derivatives 
Use foward and central difference algorithms to differentiate the functions 
    + cos(t) 
    + exp(t)
    => At t = 0.1,1,100
a) Print out the derivative and its relative error E as a functions of h. 
Reduce the step size h until it equals machine precision h ≅ e_m
b) Plot log_10|E| vs log_10h and check whether the number of decimal places agrees witht the estimate in the text 


    
"""
import os
import sys
import glob
# […]

# Libs 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import math 

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)

# section 1.3 function 

def find_overflow():
    
	previous_overflow = 1.0
	overflow = 1.0
	i = 0
	try:
		while overflow / 2.0 != overflow:
			previous_overflow = overflow
			overflow *= 2.0
			i += 1
		return (previous_overflow, i)
	except:
		return (previous_overflow, i)
	
def find_underflow():
    
	previous_underflow = 1.0
	underflow = 1.0
	i = 0
	try:
		while underflow / 2.0 != underflow:
			previous_underflow = underflow
			underflow /= 2.0
			i -= 1
		return (previous_underflow, i)
	except:
		return (previous_underflow, i)
    
def determine_machine_precision( dtype=float ):
    
	if dtype == float:
		i = 0
		e = 1
		while 1 - e != 1:
			i -= 1
			e /= 2.0
		return (i, e)
	elif dtype == complex:
		i = 0
		e = 1
		while complex( 1, 1 ) - complex( e, e ) != complex( 1, 1 ):
			i -= 1
			e /= 2.0
		return (i, e)




# Functions 

# use proper documentation once , to show I can do it . There is to much work in TP to justify doing perfect commenting on non production code . 
def function_cos(x):
    """
    Function that is being differentiated later on 
    
    Parameters
    ----------
    x : float 
        input values 

    Returns
    -------
    f : float 
        evaluates cos for that given value of x 

    """
    
    f = np.cos(x)
    return f 



def function_cos_analytic(x):
 
    f = -np.sin(x)
    return f 


def function_exp(x):
    
    f = np.exp(x)
    return f 

def function_exp_analytic(x):
    
    f = np.exp(x)
    return f 


### Difference Schemes 

def forward(x,h,n):

    if n == 1 :
        f = (function_cos(x+h)-function_cos(x))/h
    else:
        f = (function_exp(x+h)-function_exp(x))/h
  
    return f 

def central(x,h,n):

    
    if n == 1 :
        f = (function_cos(x+h)-function_cos(x-h))/(2*h)
    else:
        f = (function_exp(x+h)-function_exp(x-h))/(2*h)
  
    return f 

def error_forward(y,evaluating_value,n):
    
    
    if n == 1 :
        f = abs((forward(evaluating_value,y,1) - function_cos_analytic(1))/function_cos_analytic(1))
    else :
        f = abs((forward(evaluating_value,y,0) - function_exp_analytic(1))/function_exp_analytic(1))
    
    return f
    
def error_central(y,evaluating_value,n):
    
    if n == 1 :
        f = abs((central(evaluating_value,y,1) - function_cos_analytic(1))/function_cos_analytic(1))
    else :
        f = abs((central(evaluating_value,y,0) - function_exp_analytic(1))/function_exp_analytic(1))
   
    return f

def central_2nd_derivative(x,h,n):

    
    
    f = (function_cos(x+h)-2*function_cos(x) + function_cos(x))/h**2
    
  
    return f 

def plot_intro (x,point,function):
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    forward_line = ax1.plot(x,forward(point ,x,function), markersize=.01,label = 'forward' )
    central_line = ax1.plot(x,central(point,x,function), markersize=.01,label = 'central' )
    ax1.set_xlabel('step size : h', fontsize = 14)
    ax1.set_ylabel(r'derivative',fontsize = 14 )
    plt.title('exp(x) : x ='+str(point), fontsize = 'small')
    #plt.xlim([0, N])
    #plt.ylim([0, forward(100,x.max(),1)])
    plt.legend(loc='upper right')
    plt.savefig("d exp(x) "+str(point)+".png",dpi=800)
    plt.show()
    
def plot_a (x,point):
    #plots step size vs value of derivate at a given point 
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    forward_line = ax1.loglog(x,error_forward(x,point,1),'o', markersize=.01,label = 'forward' )
    central_line = ax1.loglog(x,error_central(x,point,1),'o', markersize=.01,label = 'central' )
    ax1.set_xlabel('step size : h', fontsize = 14)
    ax1.set_ylabel(r'relative error',fontsize = 14 )
    plt.title('cos(x) : x ='+str(point), fontsize = 'small') 
    #plt.xlim([0, N])
    #plt.ylim([0, forward(100,x.max(),1)])
    plt.legend(loc='upper right')
    #plt.savefig("High resoltion"+str(point)+".png",dpi=800)
    plt.show()
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
   
    forward_line = ax2.loglog(x,error_forward(x,point,0),'o', markersize=.01,label = 'forward' )
    central_line = ax2.loglog(x,error_central(x,point,0),'o', markersize=.01,label = 'central' )
    ax2.set_xlabel('step size : h', fontsize = 14)
    ax2.set_ylabel(r'relative error',fontsize = 14 )
    plt.title('exp(x) : x ='+str(point), fontsize = 'small')
    #plt.xlim([0, N])
    #plt.ylim([0, forward(100,x.max(),1)])
    plt.legend(loc='upper right')
    plt.savefig("High s resoltion"+str(point)+".png",dpi=800)
    plt.show()

def plot_b (x,point,h):
    #plots step size vs value of derivate at a given point 
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    
    central_line = ax1.plot(x,central_2nd_derivative(x,h,1),label = 'central' )
    ax1.set_xlabel('x values', fontsize = 14)
    ax1.set_ylabel(r'derivative',fontsize = 14 )
    plt.title('cos(x) : x =', fontsize = 'small') 
    #plt.xlim([0, N])
    #plt.ylim([0, forward(100,x.max(),1)])
    plt.legend(loc='upper right')
    #plt.savefig("High resoltion  a.png",dpi=800)
    plt.show()
   
### Section 1.3 : Dealing With Floating Point Numbers
print("\n""Section 1.3 : Dealing With Floating Point Numbers" "\n\n")  
 
print ("Overflow for this computer is :" "\n"+ str(find_overflow()[0]))
print ("Underflow for this computer is :" "\n"+ str(find_underflow()[0]))
print('Machine precision of floats: ' + str( determine_machine_precision() ) +"\n")
print('Machine precision of complex: ' + str( determine_machine_precision(complex) ) +"\n")
### Section 1.4 : Numerical Derivatives
print("Section 1.4 : Numerical Derivatives" "\n\n")
step = .01# np.arange(.01,.01,-.001)

# Inital (evaluate functions at 3 points given )
print("for cos(x) : h = "+str(step) )
print("t = 0.1 ")
print("forward:", forward(.1,step,1),"\n","relative error:" ,error_forward(step,.1,1),"\n","central:",central(.1,step, 1),"\n" ,"relative error:" ,error_central(step,.1,1)  )
print("t = 1 " )
print("forward:", forward(1,step,1),"\n","relative error:" ,error_forward(step,1,1),"\n","central:",central(1,step, 1),"\n" ,"relative error:" ,error_central(step,1,1)  )
print("t = 100 " ,)
print("forward:", forward(100,step,1),"\n","relative error:" ,error_forward(step,100,1),"\n","central:",central(100,step, 1),"\n" ,"relative error:" ,error_central(step,100,1)  )
print("\n")
print("for exp(x) : h = "+str(step) )
print("t = 0.1 ")
print("forward:", forward(.1,step,0),"\n","relative error:" ,error_forward(step,.1,0),"\n","central:",central(.1,step, 0),"\n" ,"relative error:" ,error_central(step,.1,0)  )
print("t = 1 " )
print("forward:", forward(1,step,0),"\n","relative error:" ,error_forward(step,1,0),"\n","central:",central(1,step, 0),"\n" ,"relative error:" ,error_central(step,1,0)  )
print("t = 100 " ,)
print("forward:", forward(100,step,0),"\n","relative error:" ,error_forward(step,100,0),"\n","central:",central(100,step, 0),"\n" ,"relative error:" ,error_central(step,100,0)  )

# Reducing step size until it reaches machine precison 


s = np.geomspace(determine_machine_precision()[1],1)
ss = np.geomspace(determine_machine_precision()[1],1,100000)
# Plot for each t value 
x=np.arange(determine_machine_precision()[1],1,0.0000001)
#plot_intro (ss,.1,1)
#plot_intro (ss,1,1)
#plot_intro (ss,100,1)
#plot_intro (ss,.1,0)
#plot_intro (ss,1,0)
#plot_intro (ss,100,0)
central_2nd_derivative(x,step,1)
# Relative error part 
plot_a(ss,.1)
plot_a(ss,1)
plot_a(ss,100)

h= np.finfo(float).eps

xx = np.arange(0,8*np.pi,.01)
#plot_b(xx,xx)
#x = np.arange(.0000001,.000000000000001,-.000000000000001)

#plt.loglog(x,forward(1,x,0),label='forward')

last = np.arange (determine_machine_precision()[1],np.pi/10,0.00000001)

plot_b(xx,xx,last[0])
plot_b(xx,xx,last[1])
plot_b(xx,xx,last[5])
plt.loglog(ss,error_central(ss,1,0))






