#!interpreter [optional-arg]
# -*- coding: utf-8 -*-


"""


"""

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq 
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED

# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))


# Note on logging 
# The @logg decorator was used when fine tunning the code , but is commented out currently for the final version . 
def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper

# function being solved 
def f(x,t):
    return (1 + t)*x + 1 - 3*t + t**2

# Simple Euler method 
def Euler(dt,x0,i):
    x = x0 + f(x0 , i*dt)*dt
    return x

# Improved Euler method 
def im_Euler(dt,x0,i):
    x = x0 +( f(x0, (i)*dt ) + f(x0 +f(x0 ,(i)*dt )*dt, (i+1)*dt)) * dt/2
    return x

# Fourth-Order Runge Kutta Method
def rkk(dt,x0,i):

    k1 = dt * f( x0, i*dt) 
    k2 = dt* f( x0 + 0.5 * k1, i*dt + 0.5 *dt) 
    k3 =  dt* f( x0 + 0.5 * k2, i*dt + 0.5 * dt) 
    k4 = dt * f( x0 + k3, i*dt + dt) 
    x = x0 + (1.0 / 6.0)*(k1 + 2 * k2 + 2 * k3 + k4)
    return x

@logg_non_dataframe
def array_of_solutions_to_ode(dt,x0,tf):
    
    start_x0 = x0
    
    # set the array sizes 
    t=np.linspace(0,5.,int(tf/dt))
    sim_eu = np.zeros(int(tf/dt))
    im_eu = np.zeros(int(tf/dt))
    rk = np.zeros(int(tf/dt))
    
    # loading arrays 
    for i in range(0,int(tf/dt)):
        sim_eu[i]=Euler(dt,x0,i)
        x0=sim_eu[i]
    
    x0=start_x0
    for i in range(0,int(tf/dt)):
        im_eu[i]=im_Euler(dt,x0,i)
        x0=im_eu[i]
        
    x0=start_x0
    for i in range(0,int(tf/dt)):
        rk[i]=rkk(dt,x0,i)
        x0=rk[i]
        
    
    return sim_eu , im_eu , rk ,t

@logg_non_dataframe
def plot_1_Direction_Field():
   
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111)
    
    ax.set_xlabel('Time')
    ax.set_ylabel('$x$')
    
    
    #ax.streamplot(T ,X ,tslopes ,xslopes, linewidth=0.5 ,color='k')
    #ax.plot(t,sim_eu,label='Simple Euler',linewidth=2)
    #ax.plot(t,im_eu,label='Improved Euler')
    #ax.plot(t,rk, label='Runge-Kutta')
    #ax.quiver(T ,X ,tslopes ,xslopes)
    
    ax.set_ylim(-3,3)
    ax.set_xlim(0,5)
    ax.legend()

    #plt.show()
    
    # Notes : These have been commented 
    
    ## Produce Coloured streamplot ##
    a = ax.streamplot(T ,X ,tslopes ,xslopes, linewidth=0.5 ,color=xslopes,cmap='autumn')
    b = ax.streamplot(T ,X ,tslopes ,xslopes, linewidth=0.5 ,color=tslopes,cmap='autumn')
    return 


@logg_non_dataframe
def plot_2_Simple_Euler_Method():
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111)
    ax.streamplot(T ,X ,tslopes ,xslopes, linewidth=0.5 ,color='k')
    ax.plot(t,sim_eu,label='Simple Euler, $\Delta t=0.04$')
    ax.plot(t,im_eu,label='Improved Euler, $\Delta t=0.04$')
    ax.plot(t,rk, label='Runge-Kutta, $\Delta t=0.04$')
    ax.set_xlabel('Time')
    ax.set_ylabel('$x$')
    ax.set_ylim(-3,3)
    ax.set_xlim(0,5)
    ax.legend()
    
    return 

@logg_non_dataframe
def plot_3_Comparison() :
    fig = plt.figure(figsize=(10,10))
    ax = fig.add_subplot(111)
    x0=0.0655
    
    #ax.plot(t,sim_eu,label='Simple Euler, $\Delta t=0.04$')
    ax.plot(t_2,sim_eu_2,label='Simple Euler,  $\Delta t=.0001$')
    
    #ax.plot(t,im_eu,label='Improved Euler, $\Delta t=0.04$')
    ax.plot(t_2,im_eu_2,label='Improved Euler,  $\Delta t=.0001$')
    
    #ax.plot(t,rk, label='Runge-Kutta, $\Delta t=0.04$')
    ax.plot(t_2,rk_2, label='Runge-Kutta, $\Delta t=.0001$')
    ax.plot(0,x0,'mo',label='x=0.0655', markersize=10)
    a = ax.streamplot(T ,X ,tslopes ,xslopes, linewidth=0.5 ,color=xslopes,cmap='autumn')
    b = ax.streamplot(T ,X ,tslopes ,xslopes, linewidth=0.5 ,color=tslopes,cmap='autumn')
    
    ax.set_ylim(-3,3)
    ax.grid()
    ax.legend()
    plt.savefig('Comparing ALL 3 and t = .0001 .png',dpi=500)
   
    
    return 
        
# arrays for the first 2 plots 
sim_eu , im_eu , rk , t = array_of_solutions_to_ode(0.04,0.0655,5.0)    
# arrays for the last plot 
sim_eu_2 , im_eu_2 , rk_2 , t_2 = array_of_solutions_to_ode(.0001,0.0655,5)

# Set up as to make the dirction field plot 
tt, xx = np.arange(0 ,5 ,0.2 ), np.arange(-3. ,3. ,0.24 )
T, X = np.meshgrid(tt, xx)
slope = f(X ,T )

tslopes=np.cos(np.arctan(slope))
xslopes=np.sin(np.arctan(slope))

# The functions for each section 
plot_1_Direction_Field()
plot_2_Simple_Euler_Method()
plot_3_Comparison()

print ("\nThe plots in the report where generated by changing some of the paramters within the functions.\n\nNOT ALL PLOTS ARE PRINTED OUT WHEN THE CODE RUNS , BAR THE IMPORTANT ONES ")




