{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Two-dimensional tunneling barrier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The wavefunction in the three regions is given by a Schrodinger equation\n",
    "$$ - \\frac{\\hbar^2}{2 m} \\left( \\frac{\\partial^2}{\\partial x^2} + \\frac{\\partial^2}{\\partial y^2}\\right) \\psi(x,y) = \\left(E - V(x)\\right) \\; \\psi(x,y)  $$\n",
    "where $V(x)=0$ in regions I and III, and $V(x) = V$ in region II."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In each region, we can write solutions like\n",
    "$$ \\psi (x, y) =  A \\psi(x) e^{i k_y y } $$   \n",
    "Due to translational invariance in the $y$-direction, $k_y$ is conserved across the barrier, and we can rewrite the problem only in terms of $k_x$.  \n",
    "In each region, the total wavefunction can be written as a superposition of forward and backward propagating components:\n",
    "$$ \\psi (x) =  A \\; e^{i k_x x } + B \\; e^{- i k_x x }$$ "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The wavevectors $\\mathbf{k}^I, \\mathbf{k}^{II}, \\mathbf{k}^{III}$ in the three regions obey\n",
    "$$ k = \\sqrt{k_x^2 +k_y^2} = \\frac{\\sqrt{2 m (E - V(x) )}}{\\hbar} $$so that\n",
    "\n",
    "$$ k_x^I = k_x^{III} \\equiv k_x \\qquad \\textrm{and} \\qquad k_x^{II} = \\sqrt{ k_x^2 - \\frac{2 m V}{\\hbar^2}} \\equiv q_x$$\n",
    "\n",
    "and\n",
    "\n",
    "$$k_y^I = k_y^{II} = k_y^{III} \\equiv k_y$$\n",
    "where $k_x = k \\cos(\\theta)$ and $k_y = k \\sin(\\theta)$\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To solve the scattering problem, we consider an incoming wave and reflected wave in region I, two counter-propagating waves in region II, and a transmitted wave only in region III:\n",
    "\n",
    "$$\n",
    "\\begin{aligned} \n",
    "\\psi_I (x) &= e^{i k_x x} + r e^{-i k_x x}  \\\\ \n",
    "\\psi_{II} (x) &= a e^{i q_x x} + b e^{-i q_x x}  \\\\ \n",
    "\\psi_{III}(x) &= t e^{i k_x x} \\\\ \n",
    "\\end{aligned}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Wavefunction matching:\n",
    "$$\n",
    "\\begin{aligned} \n",
    "\\psi_I (0) &= \\psi_{II} (0) \\\\\n",
    "\\psi_{II} (L) &= \\psi_{III} (L) \n",
    "\\end{aligned}\n",
    "$$\n",
    "and their derivatives"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sympy as sym           \n",
    "# can simplify expressions (no need for \"sym.\" below) using from sympy import * here\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "from IPython.display import display\n",
    "import matplotlib.cm as cm\n",
    "from matplotlib import colors\n",
    "import scipy\n",
    "sym.init_printing()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, y, L = sym.symbols('x, y, L', real=True)\n",
    "k, qx = sym.symbols('k, q_x ', complex=True)\n",
    "kx, ky = sym.symbols('k_x, k_y', real=True)\n",
    "qxr, qxi = sym.symbols('q_x^r, q_x^i', real=True)\n",
    "\n",
    "\n",
    "a, b, r, t = sym.symbols('a, b, r, t', complex=True)\n",
    "\n",
    "# Let's create a generic exponential wavefunction to use throughout\n",
    "psi = sym.exp(sym.I * k * x) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Can now create various combinations using (e.g.): \n",
    "a*psi.subs({k:kx, x:L})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The wavefunctions in the three regions can thus be written:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "psi1 = psi.subs({k:kx}) + r*psi.subs({k:-kx})\n",
    "psi2 = a*psi.subs({k:qx}) + b*psi.subs({k:-qx})\n",
    "psi3 = t*psi.subs({k:kx})\n",
    "display(psi1, psi2, psi3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From which we can solve for the wavefunction coefficients $r, a, b, t$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solutions = sym.solve([psi1.subs(x, 0) - psi2.subs(x, 0), \n",
    "          psi2.subs(x, L) - psi3.subs(x, L), \n",
    "          psi1.diff(x).subs(x,0) - psi2.diff(x).subs(x, 0),\n",
    "          psi2.diff(x).subs(x, L) - psi3.diff(x).subs(x, L)] , [r, a, b, t]  )\n",
    "t_coeff = solutions[t]\n",
    "display(t_coeff)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "display(solutions[a])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# and the transmission T = t*t\n",
    "T = sym.Abs(t_coeff**2)\n",
    "display(T)    \n",
    "\n",
    "#try with qx real\n",
    "T_qx_r = T.subs(qx, qxr).rewrite(sym.cos).simplify().trigsimp()\n",
    "display(T_qx_r)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# try with qx imaginary...\n",
    "T_qx_i=T.subs(qx, sym.I*qxi).rewrite(sym.cos).simplify().trigsimp()\n",
    "display(T_qx_i)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Now let's write things in terms of energy, potential, and incoming angle ($\\theta$)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#alpha = hbar^2 / 2m\n",
    "E, V, theta, alpha = sym.symbols('E, V, theta, alpha', real=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# create a piecewise function that returns the appropriate value of qx\n",
    "# (avoids issues with a complex square root)\n",
    "qx_gen =sym.Piecewise((sym.sqrt( kx**2  - V/alpha ), kx**2 - V/alpha >= 0 ), \n",
    "                      (sym.I*sym.sqrt( V/alpha -kx**2 ), kx**2  - V/alpha<0))\n",
    "display(qx_gen)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T_theta = T.subs({qx: qx_gen}).subs({kx: sym.sqrt(E/alpha)*sym.cos(theta)})\n",
    "#display(T_theta)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's make a function to allow us to calculate values and draw graphs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T_func = sym.lambdify([theta, E, V, L, alpha], T_theta, \"scipy\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# alpha = 0.6 eV nm^(-2) corresponds well to GaAs, where effective mass = 0.067 me\n",
    "# use eV for energy units, and nm for length in calculations below:\n",
    "alpha_GaAs = 0.6\n",
    "T_func(0, 0.24, 0.2, 10, alpha_GaAs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "incident = 0.0\n",
    "potential = 0.1\n",
    "length = 20\n",
    "\n",
    "energies = np.linspace(0.001, 0.3, 400)\n",
    "trans = T_func(incident, energies, potential, length, alpha_GaAs)\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(energies, trans)\n",
    "ax.set_ylabel('Transmission', fontsize=18)\n",
    "ax.set_xlabel('Energy', fontsize=18)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "energies = 0.05, 0.15\n",
    "incident = 0.0\n",
    "potential = 0.1\n",
    "\n",
    "lengths = np.linspace(1, 50, 101)\n",
    "trans1 = T_func(incident, energies[0], potential, lengths, alpha_GaAs)\n",
    "trans2 = T_func(incident, energies[1], potential, lengths, alpha_GaAs)\n",
    "\n",
    "\n",
    "fig, ax = plt.subplots()\n",
    "ax.plot(lengths, trans1, label=r'$E='+str(energies[0])+\"eV$\")\n",
    "ax.plot(lengths, trans2, label=r'$E='+str(energies[1])+\"eV$\")\n",
    "ax.set_ylabel('Transmission', fontsize=18)\n",
    "ax.set_xlabel('Length', fontsize=18)\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "energy = 0.15\n",
    "potential = 0.1\n",
    "length = 20\n",
    "\n",
    "angles = np.linspace(-np.pi/2, np.pi/2, 101)\n",
    "trans = T_func(angles, energy, potential, length, alpha_GaAs)\n",
    "\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111, projection='polar')\n",
    "ax.plot(angles, trans)\n",
    "ax.set_thetamin(-90)\n",
    "ax.set_thetamax(90)\n",
    "ax.set_ylabel('Transmission', fontsize=18)\n",
    "ax.set_xlabel('Angle', fontsize=18)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
