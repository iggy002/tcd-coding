#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
import random 
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
import numpy.random as random
import csv
import smopy
import imageio
from functools import wraps
 
from matplotlib import cm,colors
from scipy import constants
# […]

# Own modules
#from {path} import {class}
import SalesmanAnneal
from SalesmanAnneal import SalesmanAnneal, logg_non_dataframe
#import FUNCTIONS_TO_BE_USED

# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))

@logg_non_dataframe
def load_data_from_csv():
    # loads the data from the csv file 
    with open('../Lab 4/cities(1).csv', encoding="utf8") as csvfile:
        readCSV = csv.reader(csvfile, delimiter=',')

        #skip the first line, which contains the legend
        next(readCSV)
        
        #add elements row-wise
        for row in readCSV:
            city = row[0] # Correct 
            country = row[1]
            lat = float(row[2]) 
            long = float(row[3])
            pop= float(row[4])
            
            countrynames.append(country)
            citynames.append(city)
            lats.append(lat)
            longs.append(long) 
            pops.append(pop)
            
            if row[1] == 'IRL': # IRL
                irish_cities.append(city)
                ilats.append(lat)
                ilongs.append(long) 
            
    spops=np.copy(pops).tolist()
    spops.sort(reverse=True)
    indarr=[]
    
    for p in (spops):
        indarr.append(pops.index(p))
    
    for i in range(0,25):
        tlats.append(lats[indarr[i]])
        tlons.append(longs[indarr[i]])
        tcits.append(citynames[indarr[i]])
        tpop.append(pops[indarr[i]])
        
    return countrynames, citynames, lats, longs, pops, irish_cities,ilats,ilongs ,tcits,tlats,tlons,tpop
   
@logg_non_dataframe
def random_list_cities(num_cities):
    rands = np.random.choice(range(0,len(longs)),num_cities, replace=False)
    rlats=[lats[i] for i in rands]
    rlons=[longs[i] for i in rands]
    rcit=[citynames[i] for i in rands]
    rcon=[countrynames[i] for i in rands]
    
    return rlats, rlons, rcit, rcon

@logg_non_dataframe
def Plot_and_Details(name_instance,T,x,y):
    
    # Plots 
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    ax1.plot(x,y)
    ax1.set_ylabel('Path Lenght')
    ax1.set_xlabel('Iteration')
    ax1.set_title('Convergence of Simulated Annealing ('+str(name_instance.name)+')')
    plt.savefig('Convergence of Simulated Annealing'+str(name_instance.name)+'.png')
    print('\n'+str(name_instance.name)+'\nInitial temp=',T,'\nFinal temp=',name_instance.T*name_instance.itterlist[-1])
    

@logg_non_dataframe
def simulation( lats,lons,cities,basemap,stopping_itt,T,alpha , name): 
    
    # lats :
    # lons :
    # cities :
    # stopping_itt :
    # T :
    # alpha :
    
    a = SalesmanAnneal(lats = lats , lons = lons , cities = cities, basemap = basemap, stopping_itt= stopping_itt, T=T, alpha=alpha ,name = name )
    a.anneal()
    a.print_grid(a.initial_route)
    a.print_grid(a.bestroute)

    return a

@logg_non_dataframe
def images_for_animation(simulation_object):
    # Code for generating images to be used in gif.
    list_to_plot = simulation_object.all_route # list of all_routes to be ploted for a given instance 
    #yes = most_populous_cities.all_route
    for i in range (len(list_to_plot)) : 
        most_populous_cities.print_grid(list_to_plot[i])
    
    return

### Code that answers the question 

# smopy tile server and basic options
smopy.TILE_SERVER = "http://tile.basemaps.cartocdn.com/light_all/{z}/{x}/{y}@2x.png"
smopy.TILE_SIZE = 512

# generate world map ((lat_min, long_min, lat_max, long_max) , z=zoom_level) ajd map of ireland 
worldmap = smopy.Map((0, -120, 65, 120), z=2)
irishmap = smopy.Map((51.5, -9., 55, -5.6), z=7)


# Lists which we want to fill
countrynames, citynames, lats, longs, pops=[], [], [],[],[]
irish_cities,ilats,ilongs=[],[],[]
tcits,tlats,tlons,tpop=[],[],[],[] 

# filling them 
countrynames, citynames, lats, longs, pops, irish_cities,ilats,ilongs ,tcits,tlats,tlons,tpop = load_data_from_csv()

# Setting values 

T=1E10
num_cities=30

# Filling Arrays 
rlats, rlons, rcit, rcon  = random_list_cities(num_cities)

random_cities = simulation(rlats, rlons, rcit, worldmap, 5000,T,.97, 'random_cities')
most_populous_cities = simulation(tlats, tlons, tcits ,worldmap , 8000,T,.95, 'most_populous_cities')
irish_locations = simulation(ilats, ilongs, irish_cities,irishmap,  2000,T,.97, 'irish_locations')

Plot_and_Details(random_cities ,1E10,random_cities.itterlist,random_cities.lenght1)
#print(rcit)
Plot_and_Details(most_populous_cities ,1E10,most_populous_cities.itterlist,most_populous_cities.lenght1)
Plot_and_Details(irish_locations ,1E10,irish_locations.itterlist,irish_locations.lenght1)


# Generate images to be used in gif ( just uncomment the line below)
#images_for_animation(random_cities)

""" 
Ainimated images (an online tool works better)
folder = 'repos/tcd-coding/Numerical Methods/Lab 4/ireland gif images' 
files = [f"{folder}\\{file}" for file in os.listdir(folder)]

images = [imageio.imread(file) for file in files]
imageio.mimwrite('movie.gif', images, fps=6)

folder = 'repos/tcd-coding/Numerical Methods/Lab 4/World Largest cities gif images'
files = [f"{folder}\\{file}" for file in os.listdir(folder)]

images = [imageio.imread(file) for file in files]
imageio.mimwrite('movie main .gif', images)

"""
