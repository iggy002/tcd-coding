#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
import random 
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
import numpy.random as random
import csv
import smopy
from functools import wraps
 
from matplotlib import cm,colors
from scipy import constants


class SalesmanAnneal:
    '''Solves the TSM problem using a simulated annealing approach'''
    
    def __init__(self, lats, lons, cities, basemap,stopping_itt, T,alpha):
        '''initialise the solver, loads city coordinates/names from arguments '''
        self.lats = lats
        self.lons = lons
        self.cities=cities
        self.N= len( cities )
        self.basemap=basemap
        self.itt=1
        self.itt=1
        self.stopping_itt=stopping_itt
        self.T = T 
        self.alpha=alpha

        self.itterlist=[]
        self.lenght=[]
        
        self.lenght1=[]
        self.lst=[(i)%self.N for i in range(0,2*self.N)]
        
        self.initial_route=(np.random.permutation(np.arange(self.N)))
        self.bestroute = np.copy(self.initial_route)
        
        self.cur_fitness= self.fitness(np.copy(self.bestroute))
        self.initial_fitness = self.cur_fitness
        self.best_fitness = self.cur_fitness
        
        
    def fitness(self, candidate):
        "returns the 'fitness', i.e. total RETURN path length."
        
        fit=0
        for i in range(0,len(candidate)): 
            fit+=self.gcd(self.lats[ candidate[i] ], self.lons[ candidate[i] ], self.lats[ candidate[(i+1)%self.N] ], self.lons[candidate[(i+1)%self.N]])
        self.lenght.append(fit)
        return fit
    
        
        
    def gcd(self,alat, alon, blat, blon):
        "The length of the shortest path between a and b"
        
        lat1 = np.radians(alat)
        lat2 = np.radians(blat)
        lon1 = np.radians(alon)
        lon2 = np.radians(blon)

        dlon = lon2 - lon1 
        dlat = lat2 - lat1 

        hav = (np.sin(dlat/2))**2 + np.cos(lat1) * np.cos(lat2) * (np.sin(dlon/2))**2 
        c = 2 * np.arctan2( np.sqrt(hav), np.sqrt(1-hav) ) 
        return 6371* c 


   
    def gcd_path(self,alat, alon, blat, blon, num):
        " returns 'num' points on the shortest path between a and b "
        
        lat1 = np.radians(alat)
        lat2 = np.radians(blat)
        lon1 = np.radians(alon)
        lon2 = np.radians(blon)

        d=self.gcd(alat, alon, blat, blon)
        f= np.linspace(0, 1, num)

        delta = d / 6371
        alpha = np.sin((1-f)*delta) / np.sin(delta)
        beta = np.sin(f*delta) / np.sin(delta)

        x = alpha * np.cos(lat1) * np.cos(lon1) + beta * np.cos(lat2) * np.cos(lon2)
        y = alpha * np.cos(lat1) * np.sin(lon1) + beta * np.cos(lat2) * np.sin(lon2)
        z = alpha * np.sin(lat1) + beta * np.sin(lat2)

        newlats = (np.arctan2(z, np.sqrt(x**2 + y**2)))
        newlons = (np.arctan2(y, x))
        return np.degrees(newlats), (np.degrees(newlons) +540)%360 -180   

    
    
    def accept(self, candidate):
        
        candidate_fitness = np.copy(self.fitness( candidate ))
        
        # probability 1 for a lower energy candidate
        if candidate_fitness < self.cur_fitness:
            self.cur_fitness = candidate_fitness
            self.currentgrid = candidate
            
            #is the new candidate the best so far?
            if candidate_fitness < self.best_fitness:
                self.best_fitness = candidate_fitness
                self.bestroute = candidate
                self.itterlist.append(self.itt)
                self.lenght1.append(np.copy(self.cur_fitness))

        # otherwise accept with a probability given by the boltzmann-like term
        else:
            if np.random.random() < np.exp( - abs( candidate_fitness - self.cur_fitness) / self.T):
                self.cur_fitness = candidate_fitness
                self.currentgrid = candidate
                

            
            
    
    def print_grid(self, path):
        '''Outputs a pretty map showing a particular route'''
        px, py = [], []
        p1x, p1y = [], []
        plot1, ax = plt.subplots(figsize=(20, 17.5))

        for i in range(len(self.lats)):
            plotx, ploty = self.basemap.to_pixels(self.lats[i], self.lons[i])
            px.append(plotx)
            py.append(ploty)
                    
        for i in range(0,len(path)):
            path_lat, path_lon = self.gcd_path(self.lats[ path[i] ], self.lons[ path[i] ], self.lats[ path[(i+1)%self.N] ], self.lons[path[(i+1)%self.N]],100)
            plotx, ploty = self.basemap.to_pixels(path_lat, path_lon)
            p1x.append(plotx)
            p1y.append(ploty)
            
        ax.set_xticks([])
        ax.set_yticks([])
        ax.grid(False)
        ax.set_xlim(0, self.basemap.w)
        ax.set_ylim(self.basemap.h, 0)
        ax.axis('off')
        plt.tight_layout()
        ax.imshow(self.basemap.img)

        #plot points for each city
        ax.plot(px, py, 'ro')

        #plot great circular path
        ax.plot(p1x, p1y, 'go', ms=0.8, alpha=0.8)

        #include labels
        #[ax.annotate(self.cities[i], xy=(px[i], py[i]), xytext=(px[i]+36, py[i]+15), color='k', size=10) for i in range(0, len(self.cities))]
        plt.show(plot1)
        
    def print_inf(self):
        print(self.bestroute-self.initial_route)
        
    def anneal(self):
        '''simulated annealing to find solution'''

        while self.itt < self.stopping_itt:
            
            # generate a new candidate solution
            candidate = np.copy(self.bestroute)

            # pick two random elements and swap path
            rands = np.random.choice(range(0,self.N),2, replace=False)

            if rands[0]<rands[1]:
                indexlst=[i%self.N for i in range(rands[0], rands[1]+1)]
            else:
                indexlst=[i%self.N for i in range(rands[0], self.N+rands[1]+1)]

            rindexlst=np.copy(indexlst[::-1])
            for i,j in zip(indexlst,rindexlst):
                candidate[j]=np.copy(self.bestroute[i])

            # accept the new candidate?
            self.accept(candidate)            
            
            # update conditions
            self.T *= self.alpha
            self.itt+=1