# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 15:38:50 2020

@author: fenlo
"""
#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
import random 
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps
 
from matplotlib import cm,colors
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq 
from scipy.special import factorial
from scipy.stats import poisson
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED

# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))

def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper


@logg_non_dataframe
def Poisson_distribution_integers (mean):
    n = np.arange(0,50, 1)
    
    # Probibility 
    Poisson_distribution = poisson.pmf(n, mu=mean, loc=0) #(np.exp(-mean)*mean**n)/factorial(n) #  np.power(mean,n)*np.exp(-mean)*(1/factorial(n))
    
    return Poisson_distribution,n,mean 

@logg_non_dataframe
def Plot_Poisson_distribution (P_n,n,mean):
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    ax1.plot(n,P_n,marker='o',label = '$<n>$ :'+ str(mean) )
    ax1.bar(n,P_n, color='green')
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('n')
    ax1.legend(loc='upper right')
    ax1.set_title('Poisson distribution For $<n>$ = '+str(mean))
    plt.savefig('Poisson distribution indiviual plots Q1 '+str(mean)+'.png',dpi = 500 )
    
@logg_non_dataframe
def Plots_Poisson_distribution ():
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    for i in means : 
        P_n,n,mean = Poisson_distribution_integers(i)
        ax1.plot(n,P_n,marker='o',label = '$<n>$ :'+ str(mean) )
        ax1.bar(n,P_n)
        
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('$n$')
    ax1.legend(loc='upper right')
    ax1.set_title('Poisson Distribution for Multiple Values of $<n>$')
    plt.savefig('Poisson distributions plot Q1.png',dpi = 500 )
    
@logg_non_dataframe
def Q1_Plot_Poisson_distribution():
    
    for i in means :
        P_n,n,mean = Poisson_distribution_integers(i) # loads arrays 
        Plot_Poisson_distribution(P_n,n,mean) # plots arrays
       
    Plots_Poisson_distribution () # Plots all distribution for all 3 means 
    
    return 

def Compute_sums(mean):
    
    P_n,n,mean = Poisson_distribution_integers (mean)
    
    st = sum(P_n)
    nd = sum (P_n * n)
    rd = sum (P_n * n**2)
    Std = maths.sqrt(nd) # Standard deviation
    
    print(f"\nmean = {mean}")
    print (f"1st:{st} \n2nd:{nd}\n3rd:{rd}\nStd:{Std}")

    return st,nd,rd,Std

def Q2 ():
    # Compute sums 
    for i in means :
        Compute_sums(i)
   
    return 

means = [1,5,10,15] # array of values of means to be plotted 


def main(): 
    Q1_Plot_Poisson_distribution()
    Q2()

if __name__ == "__main__": 
    main()     