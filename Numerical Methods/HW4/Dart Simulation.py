# -*- coding: utf-8 -*-
"""
Created on Sun Dec 13 13:06:35 2020

@author: fenlo
"""
#!interpreter [optional-arg]
# -*- coding: utf-8 -*-

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
import random 
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq 
from scipy.special import factorial
from scipy.stats import poisson
# […]

# Own modules
#from {path} import {class}
#import FUNCTIONS_TO_BE_USED

# […]

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))

def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper

@logg_non_dataframe
def Poisson_distribution_integers (mean):
    n = np.arange(0,30, 1) # change to 50 normally 
    
    # Probibility 
    Poisson_distribution =  poisson.pmf(n, mu=mean, loc=0) # np.power(mean,n)*np.exp(-mean)*(1/factorial(n))
    
    return Poisson_distribution,n,mean 

@logg_non_dataframe
def Plot_Poisson_distribution (P_n,n,mean):
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    ax1.plot(n,P_n,label = '$<n>$ :'+ str(mean) )
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('n')
    ax1.legend(loc='upper right')
    ax1.set_title('Poisson distribution')
    #plt.yscale('log', nonposy='clip')
    
    #plt.show()
    
@logg_non_dataframe
def Plot_Poisson_distribution_log_scale_y (P_n,n,mean):
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    ax1.plot(n,P_n,label = '$<n>$ :'+ str(mean) )
    
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('n')
    ax1.legend(loc='upper right')
    ax1.set_title('Poisson distribution')
    #plt.yscale('log', nonposy='clip')
    
def dart_throw (regions,dart_throws,trials):
     
    L = regions # Number of regions
    
    store = np.zeros( dart_throws * trials) # stores the result of the throws 
    for i in range(0,trials):
        for i in range(0, dart_throws * trials):
            store[i] = random.randint(1,L) # if its 0 get 101 possible 
    
    return store

def Q3_method_1(regions,dart_throws,trials):
    # This uses a single array.
    # The array contains all the simulations in a singel column
    
    main_array = dart_throw (regions,dart_throws,trials)  # L regions , x throws , trials of x throws
    dataset = pd.DataFrame({'Column1': main_array})  # dataframe which is a column of all the dark throws
    aa = dataset['Column1'].value_counts().to_frame()
    aa.reset_index(inplace=True)
    aa_1 = aa['Column1'].value_counts().to_frame()
    #plt.plot(aa_1)
    
    aa['Column1'].plot.hist()
    aa_1['P'] =aa_1['Column1']/aa_1['Column1'].sum()
    aa_1.reset_index(inplace=True)
    aa_1 = aa_1.sort_values(by='index', ascending=True)
    plt.show()
    plt.plot(aa_1['index'],aa_1['P'])
    
    Simulation_dataset = pd.DataFrame({'Simulation_dataset': main_array})  # dataframe which is a column of all the dark throws = pd.DataFrame({'Column1': xx})  # dataframe which is a column of all the dark throws

    Clean_step_1 = ( Simulation_dataset['Simulation_dataset']
                                        .value_counts()
                                        .to_frame()
                                        .reset_index()
                                        .rename(columns = {'index': "Region" ,"Simulation_dataset": 'Number of Times a Dart Landed'})
                                        ["Number of Times a Dart Landed"] # now counting the number of time a dart landed x times in a particular region 
                                        .value_counts()
                                        .to_frame()
                                        .reset_index()
                                        .rename(columns = {'index': "n" ,'Number of Times a Dart Landed': 'H(n)'})
                                        .sort_values(by='n', ascending=True)
                                
                                        )
    
    Clean_step_1['P(n)'] = (Clean_step_1['H(n)']/Clean_step_1['H(n)'].sum())
    
    return Clean_step_1

def Q3_method_2 (regions,trials):
    
    data_set = [] # initailse array that will story np.arrays #
    
    trials_1 = np.arange(0,trials,1) # number of simulations 
    for i ,y   in enumerate(trials_1):
        i = dart_throw (regions,50,1) # creates an array for a single trials 
        #print (y)
        data_set.append(i)         # adds this np.array to the data_set array 

    RESULTS = pd.DataFrame(data_set).T
    
    H_n_dataframe = pd.DataFrame()  # creates a dataframe to store H(n) for each trials 
    aid = [] # create a list that will store dataframes for H(n) for each trials 
    
    tester_dataframe =pd.DataFrame()
    tester = []
    
    for i in range(0,len(data_set)): # RESULTS
        H_n_dataframe.loc[:,str(i)] = RESULTS[i] # colounn i is the ith arrray in RESULTS
        
        # transform coloumn so that is H_i(n) [ i.e H(n) for ith simulation/trials]
        i =  (H_n_dataframe[str(i)]  
                                .value_counts()
                                .to_frame()
                                .reset_index()
                                .rename(columns = {'index': "Region" ,str(i): 'Number of Times a Dart Landed'})
                                ["Number of Times a Dart Landed"] # now counting the number of time a dart landed x times in a particular region 
                                .value_counts()
                                .to_frame()
                                .reset_index()
                                .rename(columns = {'index': "n" ,'Number of Times a Dart Landed': 'H(n)'})
                                .sort_values(by='n', ascending=True)
                                
                                )
        aid.append(i)
    for j in range(0,len(data_set)): # RESULTS
        tester_dataframe.loc[:,str(i)] = RESULTS[j] # colounn i is the ith arrray in RESULTS
        
        # transform coloumn so that is H_i(n) [ i.e H(n) for ith simulation/trials]
        j =  (H_n_dataframe[str(j)]  
                                .value_counts()
                                .to_frame()
                                .reset_index()
                                .rename(columns = {'index': "Region" ,str(i): 'Number of Times a Dart Landed'})
                                
                                )
        
        tester.append( regions - len(j.Region.unique()))
        
    
    result = pd.concat(aid) # dataframe of all the H(n) stacked ontop of each other 
    H_n_final = result.groupby(['n']).sum() # dataframe of H(n) for all trials
    H_n_final = H_n_final.reset_index()
    # This deals with adding H(0) to the dataframe
    H_n_final.loc[-1] = [0, sum(tester)]
    H_n_final.index = H_n_final.index + 1  # shifting index
    H_n_final = H_n_final.sort_index() 
    H_n_final['P(n)'] = (H_n_final['H(n)']/H_n_final['H(n)'].sum())
    H_n_final['xP(n)'] = H_n_final['n']*H_n_final['P(n)']
    
    
    return H_n_final,trials

@logg_non_dataframe
def Q3_Plots(H_n_final,trials):
    
    mean_to_use = H_n_final['xP(n)'].sum()
    P_n,n,mean = Poisson_distribution_integers(mean_to_use)
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    ax1.plot(n,P_n,label = '$<n>$: '+ str(mean) )
    
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('n')
    ax1.set_title('Poisson distribution: L = 100, trials = ' +str(trials))

    ax1.plot(H_n_final['n'] ,H_n_final['P(n)'],label ="Simulation")
    ax1.legend(loc='upper right')
    #plt.savefig('Poisson distributions plot Q3.png',dpi = 500 )
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    ax2.semilogy(n,P_n,marker='o',label = '$<n>$: '+ str(mean) )
    
    ax2.set_ylabel('$P(n)$')
    ax2.set_xlabel('n')
    ax2.set_title('Poisson distribution: L = 100, trials = ' +str(trials))

    ax2.semilogy(H_n_final['n'] ,H_n_final['P(n)'],marker='o',label ="Simulation")
    #ax2.fill_between(H_n_final['n'] ,P_n,H_n_final['P(n)'] ,alpha=0.2)
    ax2.legend(loc='upper right')
    #plt.savefig('Poisson distributions plot Q3 log.png',dpi = 500 )
    
    return 

def Q3 ():
    Results_Q3 = Q3_method_2 (100,10)
    print(f"\nH(n) : Table of Values \n{Results_Q3[0]}")
    print(f" <n> = {Results_Q3[0]['xP(n)'].sum()}")
    Q3_Plots(Results_Q3[0],Results_Q3[1])
    print(Results_Q3)
    
    return 

@logg_non_dataframe
def Q4() :
    
    # Want to plot y-axis as log scale 
    """
    When
    plotting P(n) as computed from your simulation and comparing using the
    given Poisson distribution, you will notice that the values of the two
    distributions will increasingly differ with decreasing magnitude of the
    distributions. Try to quantify this roughly.
    """
    
    Results_Q4 = Q3_method_2 (100,10)
    
    mean_to_use = Results_Q4[0]['xP(n)'].sum()
    P_n,n,mean = Poisson_distribution_integers(mean_to_use)
    
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    ax1.plot(n,P_n,label = '$<n>$: '+ str(mean) )
    
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('n')
    ax1.set_title('Poisson distribution: L = 100, trials = ' +str(Results_Q4[1]))

    ax1.plot(Results_Q4[0]['n'] ,Results_Q4[0]['P(n)'],label ="Simulation")
    ax1.legend(loc='upper right')
    #plt.savefig('Poisson distributions plot Q4.png',dpi = 500 )
    
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    ax2.semilogy(n,P_n,marker='o',label = '$<n>$: '+ str(mean) )
    
    ax2.set_ylabel('$P(n)$')
    ax2.set_xlabel('n')
    ax2.set_title('Poisson distribution: L = 100, trials = 10')

    ax2.semilogy(Results_Q4[0]['n'] ,Results_Q4[0]['P(n)'],marker='o',label ="Simulation")
    ax2.legend(loc='upper right')
    #plt.savefig('Poisson distributions plot Q4 log.png',dpi = 500 )
   
    return 
def Q5_Q6_Plots(ANSWERS,Log = True):
    
    # This function is changed around with to get different plots 
    # Q5() uses len(Results[0]['P(n)'])+1
    # Q6() uses len(Results[0]['P(n)'])+5
    
    Results = ANSWERS

    mean_to_use = Results[0]['xP(n)'].sum()
    P_n,n,mean = Poisson_distribution_integers(mean_to_use)
    if Log == True:
        # Non Log
        fig1 = plt.figure() #create figure 
        ax1 = fig1.add_subplot() # create a subplot 
        
        ax1.plot(n[:len(Results[0]['P(n)'])+5],P_n[:len(Results[0]['P(n)'])+5],marker='o',label = '$<n>$: '+ str(mean) )
        
        ax1.set_ylabel('$P(n)$')
        ax1.set_xlabel('n')
        ax1.set_title('Poisson distribution: L = 5, trials = ' + str(Results[1]))
    
        ax1.plot(Results[0]['n'] ,Results[0]['P(n)'],marker='o',label ="Simulation")
        ax1.legend(loc='upper right')
        #plt.savefig('Poisson distributions plot Q6' +str(Results[1]) +'.png',dpi = 500 )
    else :
        # Log 
        
        fig2 = plt.figure() #create figure 
        ax2 = fig2.add_subplot() # create a subplot 
        
        ax2.semilogy(n[:len(Results[0]['P(n)'])+5],P_n[:len(Results[0]['P(n)'])+5],marker='o',label = '$<n>$: '+ str(mean) )
        
        ax2.set_ylabel('$P(n)$')
        ax2.set_xlabel('n')
        ax2.set_title('Poisson distribution: L = 5, trials = ' + str(Results[1]))
    
        ax2.semilogy(Results[0]['n'] ,Results[0]['P(n)'],marker='o',label ="Simulation")
        #ax2.bar(Results[0]['n'] ,Results[0]['P(n)'],color='orange')
        ax2.fill_between(n[:len(Results[0]['P(n)'])], P_n[:len(Results[0]['P(n)'])] ,Results[0]['P(n)'] ,alpha=0.2)
        ax2.legend(loc='upper left')
        #plt.savefig('Poisson distributions plot 6 log' +str(Results[1]) +'.png',dpi = 500 )
        
    print (Results[0]) # print
    
    return 

def Q5() :
    # Repeat Q3_method_2 
    # for ntrials = 100,1000,10000
    # Record the smallest value of P(n) that is simulated 
    
    trials = [1000]#,1000,10000] # ntrial to be simulated 
    ANSWERS_array = []
    for i in trials: 
        print (i)
        ANSWERS = Q3_method_2 (100,i)
        Q5_Q6_Plots(ANSWERS,1) # True yeilds non log plot 
        ANSWERS_array.append(ANSWERS)
        
    return ANSWERS_array
    
def Q6() : 
    # Repeat calculation but L = 5
    
    trials = [1000]#,1000,10000]#,100000] # ntrial to be simulated 
    ANSWERS_array = []
    for i in trials: 
        print (i)
        ANSWERS = Q3_method_2 (5,i)
        Q5_Q6_Plots(ANSWERS,1)
        ANSWERS_array.append(ANSWERS)

    return  ANSWERS_array



# Q6 all same plot
def Q6_Extra_Plot(Results_Q6):
    
    # means / expected value 
    
    #mean_to_use_0 = Results_Q6[0][0]['xP(n)'].sum()
    # mean_to_use_1 = Results_Q6[1][0]['xP(n)'].sum()
    #mean_to_use_2 = Results_Q6[2][0]['xP(n)'].sum()
    #mean_to_use_1 = Results_Q6[3][0]['xP(n)'].sum()
    
    mean_to_use_0 = Results_Q6[0][1] # These are actually trail numbers 
    mean_to_use_1 = Results_Q6[1][1]
    mean_to_use_2 = Results_Q6[2][1]
    mean_to_use_3 = Results_Q6[3][1]
    
    P_n,n,mean = Poisson_distribution_integers(10) # Plots Poisson Distrubution for dart simulation mean ( expected value)
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    ax1.semilogy(n,P_n,label = '$<n>$: ' +str(10))
    
    ax1.set_ylabel('$P(n)$')
    ax1.set_xlabel('n')
    ax1.set_title('Poisson distribution Various Trials')
    
        
    ax1.semilogy(Results_Q6[0][0]['n'] ,Results_Q6[0][0]['P(n)'],marker='o',label = 'Trial:'+str(mean_to_use_0))    
    ax1.semilogy(Results_Q6[1][0]['n'] ,Results_Q6[1][0]['P(n)'],marker='o',label = 'Trial:'+str(mean_to_use_1))   
    ax1.semilogy(Results_Q6[2][0]['n'] ,Results_Q6[2][0]['P(n)'],marker='o',label = 'Trial:'+str(mean_to_use_2))    
    #ax1.semilogy(Results_Q6[3][0]['n'] ,Results_Q6[3][0]['P(n)'],marker='o',label = 'Trial:'+str(mean_to_use_3))   
    
    #absolute_error =  abs( (P_n_est-exact))
    ax1.legend(bbox_to_anchor=(1, 1.0))
    
    #plt.savefig('NICE PLOT Q6.png',dpi = 500 )
    
    
    
    return relative_error 

def Q5_Q6_error_plots(Results,SWITCH):
    
    if SWITCH == 0 :
        P_n,n,mean = Poisson_distribution_integers(.5) 
        fig1 = plt.figure()
        ax1 = fig1.add_subplot()
        fig2 = plt.figure()
        ax2 = fig2.add_subplot()
        for i in range (0,len(Results)):
            
            a = Results[i][0]['P(n)'] - P_n[:len(Results[i][0]['P(n)'])]
            b = P_n[:len(Results[i][0]['P(n)'])]
            c = Results[i][0]['P(n)'] 
            relative_error =  abs( a/b  )
            absolute_error =  abs( c - b)
        
            ax1.plot(n[:len(Results[i][0]['P(n)'])], relative_error , label = 'Trial:'+str(Results[i][1]))
            ax2.plot(n[:len(Results[i][0]['P(n)'])], absolute_error , label = 'Trial:'+str(Results[i][1]))
        ax1.set_ylabel('Relative error')
        ax1.set_xlabel('n')
        ax1.set_title('Relative : Various Trials')
        ax1.legend(bbox_to_anchor=(1, 1.0))
        
        ax2.set_ylabel('Absolute Error')
        ax2.set_xlabel('n')
        ax2.set_title('Absolute Error : Various Trials')
        ax2.legend(bbox_to_anchor=(1, 1.0))
    else : 
        P_n,n,mean = Poisson_distribution_integers(10) 
        fig1 = plt.figure()
        ax1 = fig1.add_subplot()
        fig2 = plt.figure()
        ax2 = fig2.add_subplot()
        for i in range (0,len(Results)):
            
            a = Results[i][0]['P(n)'] - P_n[:len(Results[i][0]['P(n)'])]
            b = P_n[:len(Results[i][0]['P(n)'])]
            c = Results[i][0]['P(n)'] 
            relative_error =  abs( a/b  )
            absolute_error =  abs( c - b)
        
            ax1.plot(n[:len(Results[i][0]['P(n)'])], relative_error , label = 'Trial:'+str(Results[i][1]))
            ax2.plot(n[:len(Results[i][0]['P(n)'])], absolute_error , label = 'Trial:'+str(Results[i][1]))
        ax1.set_ylabel('Relative error')
        ax1.set_xlabel('n')
        ax1.set_title('Relative : Various Trials')
        ax1.legend(bbox_to_anchor=(1, 1.0))
        
        ax2.set_ylabel('Absolute Error')
        ax2.set_xlabel('n')
        ax2.set_title('Absolute Error : Various Trials')
        ax2.legend(bbox_to_anchor=(1, 1.0))
        
        plt.show()
    
    
    return 

#relative_error =  abs( (P_n_est-exact)/exact )
#absolute_error =  abs( (P_n_est-exact))



def main(): 
    # Q3 
    Q3() 
    # tuple of values needed for Q4
    Q4()
    Results_Q5 = Q5()
    Results_Q6 = Q6()
    Q5_Q6_error_plots(Results_Q5,0)
    Q5_Q6_error_plots(Results_Q6,1)
    
 
######################################
if __name__ == "__main__": 
    main()    

