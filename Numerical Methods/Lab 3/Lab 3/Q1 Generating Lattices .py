# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 21:34:44 2020

@author: fenlo
"""
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors,rc
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq, curve_fit

# Numba
from numba import jit

sys.path.insert(1, 'C:/Users/fenlo/repos/tcd-coding/Numerical Methods/Lab 3')

import Main 
from Main import * # imports everything from Main.py . I know this isnt best practice but I dont have enough time to make it into a packagae 




solve = IsingSolver(J=[1], dim=500, pbcs=True, starting_temp=2)
print('Initial configuration:')
fig, ax = solve.grid_figure(solve.current_moments)
plt.show()
fig2 = plt.figure(figsize=(7,7))
fig2.suptitle('Square Lattice, T=%s, Gridsize=%d'%(2,50), fontsize=16, y=1.05)


for i,state in enumerate( np.linspace(0,100000000-1,20, dtype=int) ):
    solve.run(num_its = i, temp= 5 )
    fig,ax = solve.grid_figure(solve.current_moments)
    plt.savefig(str(i)+'5.png')
    plt.show()

    
    
plt.tight_layout()
plt.show()


