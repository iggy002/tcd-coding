#!interpreter [optional-arg]
# -*- coding: utf-8 -*-


"""


"""

# Futures
#from __future__ import print_function
# […]

# Built-in/Generic Imports
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors,rc
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq, curve_fit

from numba import jit
from numba import int64, float64    # import the types
from numba.experimental import jitclass

def logg_non_dataframe(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        tic = dt.datetime.now()
        result = func(*args, **kwargs)
        time_taken = str(dt.datetime.now() - tic)
        print(f"just ran step {func.__name__} took {time_taken}s")
        return result
    return wrapper
# constant for this script 
kB = 1


class IsingSolver:
    '''a class to store data and methods for an Ising model implementation'''
    
    def __init__(self, grid_type='square', dim=100, pbcs=True, J=[1], Jrule='nn', starting_temp=1): #,field):
        '''initialises a solution to the Ising Model'''
        
        self.grid_type=grid_type
        self.dim=dim
        self.pbcs=pbcs
        self.J=J
        self.Jrule ='nn'
        
        self.neighbours = [] # List to store info on the neigbouts 
        
        # magnetic feild modification 
        #self.B = field  # calling the feild B , as its easier 
                
        #create grid
        if self.grid_type == 'square':
            self.create_square_grid()
            
        
        #random initialisation of moments
        self.init_moments = np.random.choice([-1,1], self.X.shape )
        self.current_moments = np.copy(self.init_moments)    
        self.magnetisation =  self.current_moments.flatten().sum()  # sum over all these moments 
        self.maglist = []
        self.maglist.append(self.magnetisation/ self.num_sites) # average magnetisation per site
        
        self.energy_list = []
        self.current_energy = self.grid_energy(self.current_moments)
        self.energy_list.append(self.current_energy/ self.num_sites)
        self.temp = starting_temp
        
        if self.Jrule == 'nn':
            self.sums = np.array([-4, -2, 0, 2, 4, -3, -1, 1, 3])
            self.probs = np.exp(- 2 * J[0] *self.sums / (kB * self.temp) )
            
    
        
    def grid_energy(self, moments):
        '''calculates the TOTAL energy for the full grid. Usually only done once, at the start...'''
        energy=0.0
        if self.Jrule == 'nn':
            for i, mom in enumerate(moments):
                for neighbour in self.neighbours[i]:
                    if neighbour > i:
                        energy += (-self.J[0]) * mom * moments[neighbour] 
            
        return energy

       
    def single_update(self):
        '''update by cycling through every moment in the lattice once.'''
        
        newE = self.current_energy
        if self.Jrule == 'nn':
            for i, mom in enumerate(self.current_moments):
                neighsum = np.sum(mom*self.current_moments[self.neighbours[i]])
                deltaE = 2 *self.J[0] * neighsum
                deltaM = 0
                
                if(deltaE <= 0):
                    newE += deltaE
                    self.current_moments[i] = - self.current_moments[i]
                    deltaM = 2*self.current_moments[i]
                
                if(deltaE > 0):
                    prob = self.probs[np.where(self.sums == neighsum)]
                    if np.random.random() < prob :
                        newE += deltaE
                        self.current_moments[i] = - self.current_moments[i]
                        deltaM = 2*self.current_moments[i]
                
                self.magnetisation += deltaM
                    
        self.current_energy = newE
        self.energy_list.append(newE / self.num_sites)
        self.maglist.append(self.magnetisation/ self.num_sites)
        
     
    def run(self, num_its=100, temp=-1):
        '''Sweep through the lattice multiple times.
        Update the presaved probs array if a temperature is specified.'''
        if(temp != -1):
            self.temp = temp
            if self.Jrule == 'nn':
                self.probs = np.exp(- 2 * self.J[0] *self.sums / (kB * self.temp) )
                
        for it in range(num_its):
             self.single_update()
            
        
    def grid_figure(self, moments, figsize=(8,8)):
        '''Create a figure to show a particular configuration'''
        fig, ax=plt.subplots(figsize=figsize)
        ax.set_aspect('equal')
        ax.scatter(self.X, self.Y, c=moments, marker='o', s=2000*(figsize[0]/self.dim)**2)
        ax.set_xticks([])
        ax.set_yticks([])
        ax.set_ylim(self.Y.min(), self.Y.max())
        ax.set_xlim(self.X.min(), self.X.max())
        return fig,ax
        
        
    def create_square_grid(self):
        '''Create a square grid of spins and the associated lists of neighbouring sites'''
        x, y = np.linspace(0, self.dim-1, self.dim), np.linspace(0, self.dim-1, self.dim)
        Y, X = np.meshgrid(y, x)
        self.X, self.Y = X.flatten(), Y.flatten()
        self.num_sites = self.dim * self.dim
        
        #list of neighbours for each site - different conditions with and without periodic boundaries
        for i, (x1, y1) in enumerate(zip(self.X, self.Y)):
       
            # if the lattice has periodic boundary conditions
            if self.pbcs == True:
                templist = [[ (x1-1) % self.dim , y1], [(x1+1)% self.dim , y1], [x1, (y1-1) % self.dim], [x1, (y1+1) % self.dim]]
                reallist = [int(a[0]*self.dim + a[1]) for a in templist ]
                self.neighbours.append(reallist)
                
            if self.pbcs == False:
                #if periodic boundary conditions are turned off
                templist = [[ (x1-1) , y1], [(x1+1) , y1], [x1, (y1-1) ], [x1, (y1+1) ]]
                tlist2=[]
                for element in templist:
                    if (element[0] >= 0) and (element[1] >= 0) and (element[0]< self.dim) and (element[1] < self.dim):
                        tlist2.append(element)
                reallist = [int(a[0]*self.dim + a[1]) for a in tlist2 ]
                self.neighbours.append(reallist)   



# This doesnt work 
def arrays_Q2(Temperature):
    avgmag = np.zeros_like(templist)
    aveng = np.zeros_like(templist)
    stdeng=np.zeros_like(templist)
    stdmag=np.zeros_like(templist)
    # number of equilibriation and averaging sweeps for each temperature
    testruns, countruns = 200, 1000 #first 200 runs allow for the system to equilibriate at this temperature
    # 1000 is the number of sweeps at each temperature
    for i, temp in enumerate(templist):
        #warm up and run
        solve.run(num_its = testruns + countruns, temp=Temperature, field = maglist)
        aveng[i] = np.mean(solve.energy_list[-countruns:]) # take avergae of the last countruns values 
        avgmag[i] = np.mean(np.abs(solve.maglist[-countruns:]))                 ## why the abs here?
        stdeng[i] = np.std(solve.energy_list[-countruns:])
        stdmag[i] = np.std(np.abs(solve.maglist[-countruns:]))
    
    return avgmag ,aveng ,stdeng, stdmag

