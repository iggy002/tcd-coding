# -*- coding: utf-8 -*-
"""
Created on Sun Nov 22 18:06:11 2020

@author: fenlo
"""
class Solve:

    def __init__(self, gridsize, J ,  T, stopping_iter, field):
        
        #initialising relavent variables
        self.stopping_iter = stopping_iter
        self.iteration = 1
        self.B=field
        self.J = J
        self.T = T 
    
        self.gridsize = gridsize
        self.points = self.gridsize**2

        #initialising grid
        self.current_grid = np.random.choice([-1.,1.], size=(gridsize,gridsize), replace=True)
        self.initial_grid = np.copy(self.current_grid)       

        #Defining fitness
        self.initial_fitness=np.copy(np.array(self.in_fitness(self.current_grid)))
        self.current_fitness=np.copy(np.array(self.initial_fitness))

        #initialising acceptance probabilitys for the square case
        self.prob4J, self.prob8J = np.exp( -abs( 4.*self.J )* (1/self.T)), np.exp( -abs( 8.*self.J ) * (1./self.T)) 
        
        #initialising states, energy, temp and magnetism lists for averages 
        self.states=[]
        self.states.append(np.copy(self.current_grid))
        
        self.maglist=[]
        self.maglist.append( np.copy(self.current_grid).sum() )
        
        self.enlist=[]
        self.enlist.append( self.energy(self.current_grid) )
        self.E_i=self.enlist[0]

    def in_fitness(self,grid):
        #Creating initial fitness grids to store data determining if each point will change spin or remain
        fitness_lattice=np.zeros_like(self.current_grid)
   
        for i in range(0,self.gridsize):
            for j in range(0,self.gridsize):
                fit = (2.)*self.J*grid[i,j]*(grid[(i+1)%(self.gridsize),j] + grid[(i-1)%(self.gridsize),j] + grid[i,(j+1)%(self.gridsize)] + grid[i,(j-1)%(self.gridsize)])  #energy of one state being the sum of contributions from its neighbours
                b = self.B * grid[i%(self.gridsize),j%(self.gridsize)]
                fitness_lattice[i,j] = fit + b
        
        return fitness_lattice

    
    def fitness(self, grid , i, j ):
        #Conditions for both types of lattice
        
        fit = (2.)*(self.J)*grid[i%(self.gridsize),j%(self.gridsize)]*(grid[(i+1)%(self.gridsize),j%(self.gridsize)] + grid[(i-1)%(self.gridsize),j%(self.gridsize)] + grid[i%(self.gridsize),(j+1)%(self.gridsize)] + grid[i%(self.gridsize),(j-1)%(self.gridsize)])
        b = self.B * grid[i%(self.gridsize),j%(self.gridsize)]
        fit = fit + b
        
        return fit
 

    def plot_grid(self,grid):
        fig = plt.figure(figsize=(7,7))
        ax = fig.add_subplot(111)
        
        if self.triangular == False:
            #plotting square lattice
            ax.matshow(grid,cmap=cm.Set3,vmin=-1., vmax=1.) #-1=blue, 1=yellow
            ax.set_xticks([])
            ax.set_yticks([])

            
        return fig, ax
    
    
    def accept( self, candidate, i, j ):
        '''sets the acceptance rules for a new candidate'''
        candidate_fitness = np.array(self.fitness( candidate, i, j ) )
        
        if candidate_fitness <= 0:
            self.current_grid [i,j] = (-1.)*(np.copy(candidate[i,j]))
            
        else:
            # accepting with probability e^(-b*dE) when fitness>0 
            r=np.random.random()
            # Conditions for non triangular, no magnetic field         
         
            # Conditions for non triangular and magnetic field         
            if r < np.exp( -abs( candidate_fitness * (1/self.T))):  
                self.current_grid[i,j] = (-1.)*(candidate[i,j])


    def isinginate(self):
        # Controls number of system updates
        for stop in range(0,self.stopping_iter-1):
            #sweep over the lattice    
            for j in range(0,self.gridsize):
                for i in range(0,self.gridsize):
                    #update fitness 
                    self.accept(self.current_grid ,i ,j )

             
                    self.current_fitness[i , j] = self.fitness( self.current_grid , i, j )
                    self.current_fitness[i , (j+1)%(self.gridsize)]=self.fitness( self.current_grid , i, j+1 )
                    self.current_fitness[i , (j-1)%(self.gridsize)]=self.fitness( self.current_grid , i, j-1 )
                    self.current_fitness[(i+1)%(self.gridsize) , j]=self.fitness( self.current_grid , i+1, j )
                    self.current_fitness[(i-1)%(self.gridsize) , j]=self.fitness( self.current_grid , i-1, j )
                        
                  
            #collect data every sweep
            self.maglist.append( np.copy(self.current_grid).sum())
            self.enlist.append( self.energy( np.copy(self.current_grid) ) )
            self.states.append( np.copy(self.current_grid) )

    
    def energy(self, grid): # def grid_energy 
        #gererate energy data
        en = 0
 
        for i in range(self.gridsize):
            for j in range(self.gridsize):
                a = grid[i,j]
                b = grid[(i+1)%self.gridsize, j] + grid[i,(j+1)%self.gridsize] + grid[(i-1)%self.gridsize, j] + grid[i,(j-1)%self.gridsize]
                en += ( - self.J * a * b)
        return en/2.

    def print_info(self):
        print('\ntemp=',self.T,'\niteration=',self.iteration, '\nprobability of accepting flip if del(E) = 4j, 8J =$',self.prob4J, self.prob8J,'\nNumber of sites =',self.points,'\nField =',self.B)

