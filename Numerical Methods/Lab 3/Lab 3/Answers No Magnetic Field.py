# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 21:34:44 2020

@author: fenlo
"""
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors,rc
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq, curve_fit
from PIL import Image
# Numba
from numba import jit

sys.path.insert(1, 'C:/Users/fenlo/repos/tcd-coding/Numerical Methods/Lab 3')

import Main 
from Main import * # imports everything from Main.py . I know this isnt best practice but I dont have enough time to make it into a packagae 


__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running :' ,os.path.basename(__file__))

@logg_non_dataframe
def gifs_files():
    path = './tcd-coding/Numerical Methods/Lab 3'
    
    configfiles = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(path)
        for f in files if f.endswith('.gif')]
    
    return configfiles   

@logg_non_dataframe
def images_files():
    path = 'C:/Users/fenlo/repos/tcd-coding/Numerical Methods/Lab 3'
    
    configfiles = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(path)
        for f in files if f.endswith('.png')]
    
    return configfiles   


@logg_non_dataframe
def array_files():
    path = 'C:/Users/fenlo/repos/tcd-coding/Numerical Methods/Lab 3/Arrays Q1'

    configfiles = [os.path.join(dirpath, f)
        for dirpath, dirnames, files in os.walk(path)
        for f in files if f.endswith('.npy')]
    
    return configfiles



@logg_non_dataframe
def Q1_lattice_behviour_dim_mag ():
    
    fig1 = plt.figure(figsize=(30,15))
    
    #for i in range(0,3):
    solve1=IsingSolver(J=[1], dim=10, pbcs=True, starting_temp=3)
    solve2=IsingSolver(J=[1], dim=10, pbcs=True, starting_temp=3)
    #solve3=IsingSolver(gridsize=gs[2],  J=1.,  T=Ts[i], stopping_iter=si, field=0, triangular=False ) 
    
    solve1.run(num_its = 99, temp=2.3)
    solve2.run(num_its = 99, temp=1)
    #solve2.isinginate()
    #solve3.isinginate()
    
    x=np.linspace(0,100,100)
    ax = fig1.add_subplot(111)
    ax.plot(x, np.abs(solve1.maglist),linewidth = 0.7)
    ax.plot(x, np.abs(solve2.maglist),linewidth = 0.7)
    #ax.set_title('Absolute magnetism vs states. T=%d'%(Ts[i]))
    ax.legend(bbox_to_anchor=(1., -.1), ncol = 4)
    
    ax.set_xlabel('State/Sweep')
    ax.set_ylabel('|M|')
    
   
    return
@logg_non_dataframe
def Q1_lattice_behviour_mag_eng_vs_sweepnumber(number_iterations , temperature):
    # run a certain number of sweeps at a certain temperature an,d plot the final state
   
    solve_1 = IsingSolver(J=[1], dim= 10, pbcs=True, starting_temp=1)
    solve_2 = IsingSolver(J=[1], dim= 20, pbcs=True, starting_temp=1)
    solve_3 = IsingSolver(J=[1], dim= 40, pbcs=True, starting_temp=1)
    solve_4 = IsingSolver(J=[1], dim= 60, pbcs=True, starting_temp=1)
    solve_5 = IsingSolver(J=[1], dim= 80, pbcs=True, starting_temp=1)
    solve_6 = IsingSolver(J=[1], dim= 200, pbcs=True, starting_temp=1)
    
    solve_1.run(num_its = number_iterations , temp=temperature)
    solve_2.run(num_its = number_iterations , temp=temperature)
    solve_3.run(num_its = number_iterations , temp=temperature)
    solve_4.run(num_its = number_iterations , temp=temperature)
    solve_5.run(num_its = number_iterations , temp=temperature)
    solve_6.run(num_its = number_iterations , temp=temperature)
    
    figa, axa = plt.subplots(2,figsize=(15,5)) 
    #axa[0].plot(solve11.energy_list)
    axa[0].plot(solve_1.energy_list,label='dim= 10')
    axa[0].plot(solve_2.energy_list,label='dim= 20')
    axa[0].plot(solve_3.energy_list,label='dim= 40')
    axa[0].plot(solve_4.energy_list,label='dim= 60')
    axa[0].plot(solve_5.energy_list,label='dim= 80')
    axa[0].plot(solve_6.energy_list,label='dim= 200')
    axa[0].set_xlabel('Sweep num)ber')
    axa[0].set_ylabel('Energy per site')
    axa[0].legend(bbox_to_anchor=(1.1, 1.05))
    axa[1].plot(solve_1.maglist,label='dim= 10')
    axa[1].plot(solve_2.maglist,label='dim= 20')
    axa[1].plot(solve_3.maglist,label='dim= 40')
    axa[1].plot(solve_4.maglist,label='dim= 60')
    axa[1].plot(solve_5.maglist,label='dim= 80')
    axa[1].plot(solve_6.maglist,label='dim= 200')
    axa[1].set_xlabel('Sweep number')
    axa[1].set_ylabel('Magnetisation per site') # as a function of sweep number 
    axa[1].legend(bbox_to_anchor=(1.1, 1.05))
    plt.tight_layout()
    plt.savefig('Sweep vs M and E iterations'+str(number_iterations)+' Temp' + str(temperature) +'.png')
    
    return 

    
@logg_non_dataframe
def arrays_Q1():
    """
    You need to define templist and templist1 globally if your going to do this 
    and you need to make sure that the you comment out 
    'loading_in_arrays(Option)' function 

    """
    avgmag = np.zeros_like(templist)
    aveng = np.zeros_like(templist)
    stdeng=np.zeros_like(templist)
    stdmag=np.zeros_like(templist)
    # number of equilibriation and averaging sweeps for each temperature
    testruns, countruns = 200, 1 #first 200 runs allow for the system to equilibriate at this temperature
    # 1000 is the number of sweeps at each temperature
    # 300 for dim = 40 
    for i, temp in enumerate(templist):
        #warm up and run
        solve.run(num_its = testruns + countruns, temp=temp)
        aveng[i] = np.mean(solve.energy_list[-countruns:]) # take avergae of the last countruns values 
        avgmag[i] = np.mean(np.abs(solve.maglist[-countruns:]))                 ## why the abs here?
        stdeng[i] = np.std(solve.energy_list[-countruns:])
        stdmag[i] = np.std(np.abs(solve.maglist[-countruns:]))
    
    return avgmag ,aveng ,stdeng, stdmag


@logg_non_dataframe
def loading_in_arrays(Option):
    """
    Loads in arrays to be plotted . Option == 0 calls a function to calculate the arrays. Otherwise pre calulated arrays are used  
    
    # templist_0 : []
    """
    if Option == 0:
        print ( "you need to change the script\navgmag ,aveng ,stdeng, stdmag = arrays_Q1() \nAnd define the arrays need in arrays_Q1()\nAnd need to choose the 'dim = 'within IsingSolver() \nTHE SCRIPT WILL HAS STOPPED RUNNING ")
        return sys.exit()
        
    
    if Option == 1: # Dim = 10, 1.5-3.6 1000 steps
        avgmag = np.load(configfiles[4]) 
        aveng  = np.load(configfiles[5])
        stdeng = np.load(configfiles[6])
        stdmag = np.load(configfiles[7])    
        templist = np.linspace( 3.6,1.5, 1000)
        templist1 = np.linspace( 1.5, 3.6, 1000)
    
    if Option == 2: # Dim = 20, 1.5-3.6 1000 steps
        avgmag = np.load(configfiles[8]) 
        aveng  = np.load(configfiles[9])
        stdeng = np.load(configfiles[10])
        stdmag = np.load(configfiles[11])
        templist = np.linspace( 3.6,1.5, 1000)
        templist1 = np.linspace( 1.5,3.6 ,1000)
        
    if Option == 3: # Dim =40 , 1.5-3.6 1000 steps
        avgmag = np.load(configfiles[0]) 
        aveng  = np.load(configfiles[1])
        stdeng = np.load(configfiles[2])
        stdmag = np.load(configfiles[3])
        templist = np.linspace( 3.6,1.5, 1000)
        templist1 = np.linspace( 1.5,3.6 ,1000)
    
    if Option == 4:         # Dim =10 , 0.1-7 400 steps
        avgmag = np.load(configfiles[12]) 
        aveng  = np.load(configfiles[13])
        stdeng = np.load(configfiles[14])
        stdmag = np.load(configfiles[15])
        templist = np.linspace( 7,.1, 400 )
        templist1 = np.linspace( .1,7 ,400 )
        
    if Option == 5:         # Dim =10, 0.1 -7 1000 steps
        avgmag = np.load(configfiles[16]) 
        aveng  = np.load(configfiles[17])
        stdeng = np.load(configfiles[18])
        stdmag = np.load(configfiles[19])
        templist = np.linspace( 7,.1, 1000 )
        templist1 = np.linspace( .1,7 , 1000 )
        
        
        
    
        
        
        
    return avgmag ,aveng ,stdeng, stdmag,templist, templist1

@logg_non_dataframe
def Q1_Main_plot():
    # Main fig 
    fig = plt.figure(figsize=(20,14))
    
    # <M> plus fit 
    ax1 = fig.add_subplot(221)
    ax1.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax1.plot(templist, fit1,'r--',linewidth=1.0,label='Fit')
    ax1.plot(templist, avgmag, 'rs',markersize =3.0, label='datapoints')
    
    ax1.set_xlabel('Temperature ($k_b T$)\n (a)'    ,fontsize=14)
    ax1.set_ylabel('Average Magnetism, ($< m >$)'   ,fontsize=16)
    ax1.set_title('Average Magnetism vs Temperature',fontsize=16)
    ax1.legend()
    
    # <E> plus fit 
    ax2 = fig.add_subplot(222)
    ax2.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax2.plot(templist, fit2,'r--',linewidth=1.0, label='Fit')
    ax2.plot(templist, aveng, 'rs',markersize =3.0, label='datapoints')
    
    ax2.set_xlabel('Temperature ($k_b T$) \n (b)',fontsize=14)
    ax2.set_ylabel('Average energy, ($< E >$)'   ,fontsize=16)
    ax2.set_title('Average Energy vs Temperature',fontsize=16)
    ax2.legend()
    
    
    # <M> plus error bars 
    ax3 = fig.add_subplot(223)
    ax3.plot(templist, fit1,'r--',linewidth=1.0,label='Fit')
    ax3.plot(templist, avgmag, 'rs',markersize =3.0, label='datapoints')
    ax3.errorbar(templist, avgmag, yerr=stdmag, fmt='o', capsize=4)
    
    ax3.set_xlabel('Temperature ($k_b T$) \n (b)',fontsize=14)
    ax3.set_ylabel('Average Magnetism, ($< m >$)'   ,fontsize=16)
    ax3.set_title('Average Magnetism vs Temperature (error bars)',fontsize=16)
    ax3.legend()
    
    
    
    # <E> plus fit 
    ax4 = fig.add_subplot(224)
  
    #ax4.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax4.plot(templist, fit2,'r--',linewidth=1.0 ,label='Fit')
    ax4.plot(templist, aveng, 'rs',markersize =3.0, label='datapoints')
    ax4.errorbar(templist, aveng, yerr=stdeng, fmt='o', capsize=4)
    
    ax4.set_xlabel('Temperature ($k_b T$) \n (b)',fontsize=14)
    ax4.set_ylabel('Average energy, ($< E >$)'   ,fontsize=16)
    ax4.set_title('Average Energy vs Temperature (error bars)',fontsize=16)
    ax4.legend()
    
    
    
    
    
    plt.tight_layout()
    #plt.savefig('Main Plot Q1 (As a function of Temperature)'+str(Option)+'.png')# ,dpi=500)

@logg_non_dataframe
def Q1_Cv_plot():
    fig = plt.figure(figsize=(20,10))
    

    # C_v 
    ax3 = fig.add_subplot(121)
    ax3.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax3.plot(templist, C_v, 'rd',markersize =3.0,label='datapoints')
    ax3.set_xlabel('Temperature ($k_b T$) \n (c)',fontsize=12)
    ax3.set_ylabel('Heat Capacity, ($C_vk_b$)',fontsize=12)
    ax3.set_title('Heat Capacity vs Temperature',fontsize=15)
    ax3.legend()
    
     # X: Susceptibility
    ax4 = fig.add_subplot(122)
    ax4.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax4.plot(templist, X, 'rd',markersize =3.0,label='datapoints')
    ax4.set_xlabel('Temperature ($k_b T$) \n (d)',fontsize=12)
    ax4.set_ylabel('Magnetic Susceptibility, ($\chi$)',fontsize=12)
    ax4.set_title('Magnetic Susceptibility vs Temperature',fontsize=15)
    ax4.legend()
    #plt.savefig('Cv Q1 (As a function of Temperature) Main plot'+str(Option)+'.png')

    return

@logg_non_dataframe
def Q1_Cv_Extra_plot():
    
    templist = np.linspace( 3.6,1.5, 1000)

    
    fig = plt.figure(figsize=(15,7))
    # Dim = 10, 1.5-3.6 1000 steps
    stdeng = np.load(configfiles[6])
    C_v1 = stdeng**2/templist**2
    ax2 = fig.add_subplot(111)
    ax2.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax2.plot(templist, C_v1,markersize =3.0, label='Dim = 10')
    
    # Dim = 20, 1.5-3.6 1000 steps
    stdeng = np.load(configfiles[10])
    C_v1 = stdeng**2/templist**2
    ax2.plot(templist,C_v1,markersize =3.0, label='Dim = 20')
    
    # Dim =40 , 1.5-3.6 1000 steps
    stdeng = np.load(configfiles[2])
    C_v1 = stdeng**2/templist**2
    ax2.plot(templist,C_v1,markersize =3.0, label='Dim = 40')
    ax2.set_xlabel('Temperature ($k_b T$)\n (a)'    ,fontsize=14)
    ax2.set_ylabel('Heat Capacity, ($C_vk_b$)'   ,fontsize=16)
    ax2.set_title('Heat Capacity, ($C_vk_b$) vs Temperature',fontsize=16)
    ax2.legend()    
    #plt.savefig('Cv Q1 (As a function of Temperature) extra.png')
    
    return     

@logg_non_dataframe
def stdeng_plot():
    fig = plt.figure(figsize=(15,7))

    # Dim = 10, 1.5-3.6 1000 steps
    stdmag = np.load(configfiles[7])
    templist = np.linspace( 3.6,1.5, 1000)
    ax1 = fig.add_subplot(111)
    ax1.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax1.plot(templist, stdmag,markersize =3.0, label='Dim = 10')
    
    # Dim = 20, 1.5-3.6 1000 steps
    stdmag = np.load(configfiles[11])
    ax1.plot(templist, stdmag,markersize =3.0, label='Dim = 20')
    
    # Dim =40 , 1.5-3.6 1000 steps
    stdmag = np.load(configfiles[3])
    ax1.plot(templist, stdmag,markersize =3.0, label='Dim = 40')
    ax1.set_xlabel('Temperature ($k_b T$)\n (a)'    ,fontsize=14)
    ax1.set_ylabel('$\sigma$'   ,fontsize=16)
    ax1.set_title('$\sigma$ of $<M>$ vs Temperature',fontsize=16)
    ax1.legend()    
    #plt.savefig('stdmag Q1 (As a function of Temperature).png')
    
    fig = plt.figure(figsize=(15,7))
    # Dim = 10, 1.5-3.6 1000 steps
    stdeng = np.load(configfiles[6])
    ax2 = fig.add_subplot(111)
    ax2.axvline(x=2.269, color='r',label='$k_bT_c=2.269$')
    ax2.plot(templist, stdeng,markersize =3.0, label='Dim = 10')
    
    # Dim = 20, 1.5-3.6 1000 steps
    stdeng = np.load(configfiles[10])
    ax2.plot(templist, stdeng,markersize =3.0, label='Dim = 20')
    
    # Dim =40 , 1.5-3.6 1000 steps
    stdeng = np.load(configfiles[2])
    ax2.plot(templist, stdeng,markersize =3.0, label='Dim = 40')
    ax2.set_xlabel('Temperature ($k_b T$)\n (a)'    ,fontsize=14)
    ax2.set_ylabel('$\sigma$'   ,fontsize=16)
    ax2.set_title('$\sigma$ of $<E>$ vs Temperature',fontsize=16)
    ax2.legend()    
    #plt.savefig('stdeng Q1 (As a function of Temperature).png')
    
    return 

### Models to fit the curves 
#stretched exponential
def f1(x,a,b,c,d):  
    model = np.exp(-b*(x**(-c))) + d 
    return model

#tanh exponential
def f2(x,a,b,c,d):  
    model = a * np.tanh(-b*x+c) + d
    return model

# Fit 
@logg_non_dataframe
def fit_curves() :
    bestvals1, co = curve_fit(f1, templist, avgmag)
    fit1 = f1(templist,*bestvals1)
    
    # If I didnt do this trick the fit2 was just a straight horizontal line... No idea why and dont have the time to workout why ( the joys of TP)
    bestvals2, co1 = curve_fit(f2, templist1, aveng)
    fit2 = f2(templist1,*bestvals2)

    
    return fit1 ,fit2 ,bestvals1 ,bestvals2 

@logg_non_dataframe
def estimate_the_crxitical_temperature(fit):
    
    a,b,c,d = 0,0,0,0
    if fit == 'fit1':
        a,b,c,d = bestvals1
        #np.exp(-b*(x**(-c))) + d 
    if fit == 'fit2':
        a,b,c,d = bestvals2
    
    return a,b,c,d 

@logg_non_dataframe
def print_fit_values ():
    print("\nFor: np.exp(-b*(x**(-c))) + d")
    print("a:", a_1 ,"\nb:",b_1,"\nc:",c_1,"\nd:",d_1)
    print("\nFor: a * np.tanh(-b*x+c) + d")
    print("a:", a_2 ,"\nb:",b_2,"\nc:",c_2,"\nd:",d_2,"\n")

@logg_non_dataframe
def Table_of_Tc_values():
    print("\nSee Below Table of Estimates of Tc")
    table = pd.read_csv("Fit - Sheet1.csv")
    pd.set_option("display.max_rows", None, "display.max_columns", None)
    print (table)
    return table 
    

 

@logg_non_dataframe
def ask_user_files():
    print ("Do you want to see the list of all the gifs and images generated in this lab?\nI recommed 'No' and ask you to go checkout my repo on bitbucket:\n https://bitbucket.org/iggy002/tcd-coding/src/master/Numerical%20Methods/Lab%203/")
    print ("\nYou will need to change the file paths in the following function: \n\n gifs_files()\n images_files()\n array_files()")
    val = input("Yes or No: ") 
    print(val)
    return val 

# Showing all the images and gifs 
@logg_non_dataframe
def start():
    if str(ask_user_files()) == 'Yes' :
        gifs= gifs_files()
        img = Image.open(gifs[0])
        img.show()
        for i in range(len(gifs)):
            print (gifs[i])
        
        images= images_files()
        img = Image.open(images[0])
        img.show()
        for i in range(len(images)):
            print (images[i])
    return 

# asks user what arrays they want to see plotted 
@logg_non_dataframe
def ask_user():
    print ("\n0: Generate Arrays\n1: Dim = 10, 1.5-3.6 1000 steps \n2: Dim = 20, 1.5-3.6 1000 steps\n3: Dim = 40, 1.5-3.6 1000 steps\n4: Dim = 10 , 0.1-7 400 steps\n5: Dim = 10, 0.1 -7 1000 steps")

    val = input("Enter your value: ") 
    print(val)
    return val 


start() 
# Loading in an instance of the Class for the simulation 
solve = IsingSolver(J=[1], dim=10, pbcs=True, starting_temp=1)
print('Initial configuration:')
fig, ax = solve.grid_figure(solve.current_moments)


# creat object that will be used to load arrays in "loading_in_arrays" function .
configfiles = array_files()


# Has to be changed each time in the script 
Option = int(ask_user())

# Generate arrays instead of using preloaded 
avgmag ,aveng ,stdeng, stdmag,templist, templist1 = loading_in_arrays(int (Option))

# Define Heat Capcity and Susceptibility 
C_v = stdeng**2/templist**2#heat cap
X = stdmag**2/templist #mag sus

# Generate the fits for the curves 
fit1,fit2,bestvals1 ,bestvals2 = fit_curves() 
#print (bestvals1 ,bestvals2)
# Generate Plots for 3.1 A)
Q1_Main_plot()

Q1_Cv_plot()

Q1_Cv_Extra_plot()

stdeng_plot()

# Coefficents for fits 
a_1,b_1,c_1,d_1 =estimate_the_crxitical_temperature('fit1')
a_2,b_2,c_2,d_2 =estimate_the_crxitical_temperature('fit2')
# Prints out fit and the values generated for the fit 
print_fit_values ()
table = Table_of_Tc_values()


"""
# Saved Arrays 
np.save('stdmag dim 30 200, 1000,3.6,1.5, 1000.npy', stdmag) 
np.save('stdeng dim 30 200, 1000,3.6,1.5, 1000.npy', stdeng)
np.save('aveng dim 30 200, 1000,3.6,1.5, 1000.npy', avgmag)
np.save('avgmag dim 30 200, 1000,3.6,1.5, 1000.npy', aveng)
"""
