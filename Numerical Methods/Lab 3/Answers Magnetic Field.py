# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 21:34:44 2020

@author: fenlo
"""
import os
import sys
import glob
import datetime as dt
# […]

# Libs 
import pandas as pd 
import numpy as np
import matplotlib.pyplot as plt 
import math as maths
from functools import wraps

from matplotlib import cm,colors,rc
from scipy import constants
from scipy.optimize import minimize_scalar,leastsq, curve_fit
from PIL import Image
# Numba
from numba import jit

sys.path.insert(1, 'C:/Users/fenlo/repos/tcd-coding/Numerical Methods/Lab 3')

import Main_Magnetic_Field_Final
from Main_Magnetic_Field_Final import * # imports everything from Main_Magnetic_Field_Final.py . I know this isnt best practice but I dont have enough time to make it into a packagae 


__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)
print('\n')
print('Script That Is Running ' ,os.path.basename(__file__))



    
# Does not work due to a truth values issue so have to use another method 
@logg_non_dataframe
def arrays_Q2(Temperature):

    avgmag = np.zeros_like(mag_list)
    aveng = np.zeros_like(mag_list)
    
    # number of equilibriation and averaging sweeps for each temperature
    countruns = 1 #first 200 runs allow for the system to equilibriate at this temperature
 
    solve_mag = IsingSolver(J=[1], dim=10, pbcs=True, starting_temp=1,startingb=mag_list[0])
    for i, temp in enumerate(mag_list):
        #warm up and run
        solve_mag.run(num_its = countruns, temp=Temperature , b = mag_list)
        aveng[i] = np.mean(solve.energy_list[-countruns:]) # take avergae of the last countruns values 
        avgmag[i] = np.mean(solve.maglist[-countruns:])                 ## why the abs here?
        
    m0, mf, magsteps = -2., 2., 5
    mag_list=np.linspace(m0,mf,magsteps)
    
    solve = IsingSolver(J=[1], dim=10, pbcs=True, starting_temp=1,startingb = -1)
    m0, mf, magsteps = -2., 2., 5
    mag_list=np.linspace(m0,mf,magsteps)
    
    ks=np.linspace(0,1,200) 
    bs=2*np.sin(ks*2.5*np.pi)
    save=[]
    ts=[1,2,3,4]
    for t in ts:
        hist = IsingSolver(J=[1], dim=30, pbcs=True, starting_temp=t,startingb=bs[0]) #dim = 100 is sweat spot  . Show 20 vs 10 vs 100 
        k=0
        for i,b in enumerate(bs):
            hist.run(num_its = 1, temp=t,b=b)
            k+=1
            
            
        save.append(hist.maglist[np.where(ks>0.2)])
        plt.plot(bs[np.where(ks>0.2)],hist.maglist[np.where(ks>0.2)],'o',label=('temp=',t))
    plt.xlabel('H')
    plt.ylabel('Magnetisation per site')
    plt.legend()
    plt.show()
    
    return avgmag ,aveng 


@logg_non_dataframe
def Q2_Main_plot():
    # Main fig 
    fig = plt.figure(figsize=(10,7))
    
    # <M> 
    ax1 = fig.add_subplot(221)
    
    ax1.plot(mag_list, avgmag, 'rs',markersize =3.0, label='datapoints')
    
    ax1.set_xlabel('Applied Magnetic Field ($H$)\n (a)'    ,fontsize=14)
    ax1.set_ylabel('Average Magnetism, ($< m >$)'   ,fontsize=16)
    ax1.set_title('Average Magnetism vs Applied Magnetic Field',fontsize=16)
    #ax1.set_ylim(0,1.1)
    ax1.legend()
    
    # <E> 
    ax2 = fig.add_subplot(222)

    ax2.plot(mag_list, aveng, 'rs',markersize =3.0, label='datapoints')
    
    ax2.set_xlabel('Applied Magnetic Field ($H$)\n (b)',fontsize=14)
    ax2.set_ylabel('Average energy, ($< E >$)'   ,fontsize=16)
    ax2.set_title('Average Energy vs Applied Magnetic Field',fontsize=16)
    
   

    plt.tight_layout()


# asks user what arrays they want to see plotted 
@logg_non_dataframe
def ask_user():
    print ("\n0: Generate Arrays\n1: Dim = 10, 1.5-3.6 1000 steps \n2: Dim = 20, 1.5-3.6 1000 steps\n3: Dim = 10 , 0.1-7 400 steps\n4: Dim = 10, 0.1 -7 1000 steps")

    val = input("Enter your value: ") 
    print(val)
    return val 



solve = IsingSolver(J=[1], dim=10, pbcs=True, starting_temp=1,startingb = -1)
m0, mf, magsteps = -2., 2., 5
mag_list=np.linspace(m0,mf,magsteps)

ks=np.linspace(0,1,800) 
bs=2*np.sin(ks*2.5*np.pi)
save=[]
ts=[1,2,3,4,5,6]
for t in ts:
    hist = IsingSolver(J=[1], dim=100, pbcs=True, starting_temp=t,startingb=bs[0]) #dim = 100 is sweat spot  . Show 20 vs 10 vs 100 
    for i,b in enumerate(bs):
        hist.run(num_its = 1, temp=t,b=b) # does a single loop 
        
    save.append(hist.maglist) # for some reason I cant use the other method for loading the arrays . Get an error for ambiquity 
    plt.plot(bs,hist.maglist,'o',label= 'temp = '+str(t))
plt.xlabel('H')
plt.ylabel('Average Magnetism, ($< m >$)')
plt.title('Average Energy vs Applied Magnetic Field')
plt.axhline(color='black', lw=0.3)
plt.axvline(color='black', lw=0.3)
plt.legend()
plt.show()

# Better graph with smaller points 
x=bs
for i in range(6):
    plt.plot(x,save[i],'.',label='temp = '+str(i +1))
plt.legend()
plt.xlabel('Applied field')
plt.ylabel('Magnetisation per site')
plt.show()





