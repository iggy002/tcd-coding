{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basics of a matplotlib plot\n",
    "\n",
    "How to set up a figure, add axes, and plot data.\n",
    "\n",
    "<br>\n",
    "First, import numpy and matplotlib"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "# when using notebooks, it is useful to put global imports, definitions that will be used throughout the notebook \n",
    "# in a cell near the top, so that it only needs to be run once at the beginning.\n",
    "# don't forget to rerun it if you need to restart the notebook!\n",
    "\n",
    "# we could also put change settings here, for example, to set the default fontsize in every graph in the notebook, etc."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Figure objects are created using plt.figure(), and then we can add an axes object using the add_subplot() method.\n",
    "\n",
    "Data is plotted using the axes method .plot() which returns a list of objects representing the plotted lines.\n",
    "\n",
    "The figure is drawn by calling plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "fig1 = plt.figure()\n",
    "\n",
    "ax1 = fig1.add_subplot(111)                                    #111 = 1 row, 1 column, 1st axes\n",
    "\n",
    "x1 = np.linspace(-2, 2, 51)\n",
    "\n",
    "line_cosine, = ax1.plot(x1, np.cos(x1))                        # cos (x)\n",
    "line_taylor, = ax1.plot(x1, 1.0 - x1**2/2)                     # Taylor approx.\n",
    "\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the above example, we gave everything a name. \n",
    "\n",
    "This is is good practice: it allows us to easily change the properties of the different elements of the plot later.\n",
    "\n",
    "However, it is not strictly necessary.\n",
    "We could have achieved a similar output using a more minimal approach, and without explicitly adding an axes object:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Minimalist approach\n",
    "plt.figure()\n",
    "plt.plot(x1, np.cos(x1))                        \n",
    "plt.plot(x1, 1.0 - x1**2/2)                     \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br><br>\n",
    "\n",
    "## Basic customisation\n",
    "\n",
    "How to change line properties, axes properties, titles, legends etc.\n",
    "\n",
    "Almost any imaginable property can be changed using additional arguments within ax.plot(), or by calling methods associated with the figure, axes or individual elements\n",
    "\n",
    "Useful: use ? and tab to view possible options"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig3 = plt.figure(figsize=(10,7), facecolor='white')\n",
    "\n",
    "ax3 = fig3.add_subplot(111, facecolor=\"white\")                                    \n",
    "\n",
    "# changing line or marker properties within ax.plot()\n",
    "# ax.plot has a number of optional arguments []\n",
    "# ax.plot( [x], y, [fmt], [**kwargs])\n",
    "# fmt (\"format\") allows basic line and symbol types to be set using shortcuts\n",
    "# kwargs (\"keyword arguments\") allow you to set properties using property = value\n",
    "\n",
    "\n",
    "# setting line properties using the fmt shortcuts\n",
    "line_cosine3, = ax3.plot(x1, np.cos(x1), 'b-o', label='Cosine')                 # b = blue; - = solid line; o = circular marker\n",
    "\n",
    "# setting line properties using keyword arguments\n",
    "line_taylor3, = ax3.plot(x1, 1.0 - x1**2/2, linestyle='dashed', color='red', marker='s', markerfacecolor=\"green\") \n",
    "\n",
    "\n",
    "# line properties can also be set using a series of .set_ methods\n",
    "line_taylor3.set_label('Taylor series '+r'$1 - \\frac{x^2}{2}$')             #note use of Latex\n",
    "line_cosine3.set_markersize(10)\n",
    "\n",
    "#fill between two curves\n",
    "ax3.fill_between(x1, 1.0 - x1**2/2, np.cos(x1), facecolor='lightgrey')\n",
    "\n",
    "\n",
    "# axes properties - usually changed using ax.set_.....\n",
    "ax3.set_ylabel('the y-axis', fontsize=16)\n",
    "ax3.set_xlabel('the x-axis', fontsize=16)\n",
    "ax3.set_ylim(-1.25, 1.25)                         #range of y-axis\n",
    "ax3.set_xticks(np.linspace(-2.0, 2.0, 9))         #specify tick positions on the x-axis\n",
    "#ax3.set_yticklabels([])                          #remove labels from the y-axis (or set specific strings)\n",
    "\n",
    "# show the legend (for every object with a label)\n",
    "ax3.legend(fontsize=14, loc='lower center')\n",
    "\n",
    "plt.show()\n",
    "\n",
    "#save a figure to file\n",
    "fig3.savefig('figure3.png', bbox_inches='tight', dpi=200)\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that line properties such as markersize, color, etc do not need to be fixed.\n",
    "Using plt.scatter(), instead of plt.plot() allows marker properties to be set individually\n",
    "\n",
    "You can pass an array or list of values -- this is useful for creating scatter plots where the size / shape / color of the marker also contains information about a third dimension\n",
    "\n",
    "ax.text() and ax.annotate() can be used to add text, or text and arrow, to the plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig4 = plt.figure(figsize=(10,7))\n",
    "\n",
    "ax4 = fig4.add_subplot(111)                                    \n",
    "\n",
    "\n",
    "x4 = np.random.rand(51)\n",
    "y4 = np.random.rand(51)\n",
    "z4 = 300*np.random.rand(51)\n",
    "\n",
    "ax4.scatter(x4, y4, s=z4, c=z4)       #scatter has slightly different notation: s is markersize (ms or markersize in plt.plot)\n",
    "\n",
    "\n",
    "#annotating - note that arrowprops accepts a dictionary \n",
    "ax4.annotate('The third point', xy=(x4[2], y4[2]), xytext=(x4[2]+0.1, y4[2]+0.1), arrowprops={'arrowstyle':'->', 'color':'red'}, color='green')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercise:\n",
    "\n",
    "How do you insert gridlines? logarithmic axes? error bars?\n",
    "\n",
    "Use the examples on matplotlib.org for inspiration..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<br>\n",
    "<br>\n",
    "\n",
    "## Multiple subplots\n",
    "\n",
    "The .add_subplot(abc) or .add_subplot(a,b,c) method can be used to add multiple subplots to a single figure.\n",
    "\n",
    "In this notation a is the total number of rows, b is the total number of columns, and c is the index of the subplot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig5 = plt.figure()\n",
    "\n",
    "letters = [['A', 'B'], ['C', 'D'], ['E', 'F']]\n",
    "\n",
    "numrows, numcols = 3,2\n",
    "\n",
    "for row in range(numrows):\n",
    "    for col in range(numcols):\n",
    "        ax = fig5.add_subplot(numrows, numcols, (row)*numcols + col + 1)\n",
    "        ax.set_xticks([])\n",
    "        ax.set_yticks([])\n",
    "        ax.text(0.25, 0.25, str((row)*numcols + col + 1)+'.\\nPanel '+letters[row][col], fontsize=14)\n",
    "        \n",
    "fig5.tight_layout()  #arranges the subplots so they don't overlap\n",
    "\n",
    "#fig5.subplots_adjust can be used to manually adjust spacing and size\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that we do not need to use the same a,b,c for each subplot..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig6 = plt.figure()\n",
    "\n",
    "letters2 = ['A', 'B','C', 'D']\n",
    "\n",
    "ax = fig6.add_subplot(1, 2, 1)\n",
    "ax.set_xticks([])\n",
    "ax.set_yticks([])\n",
    "ax.text(0.25, 0.25, 'Panel '+letters2[0], fontsize=14)\n",
    "\n",
    "for row in range(3):\n",
    "        ax = fig6.add_subplot(3, 2, (row)*2 + 2)\n",
    "        ax.set_xticks([])\n",
    "        ax.set_yticks([])\n",
    "        ax.text(0.25, 0.25, 'Panel '+letters2[row+1], fontsize=14)\n",
    "        \n",
    "fig6.tight_layout() \n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Can also use plt.subplots() instead of plt.figure() to define a table of subplots.\n",
    "\n",
    "Note that here the figure and axes objects are created together"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nrows=4\n",
    "fig7, axes = plt.subplots(nrows, figsize=(8,nrows*1.5))     #creates a figure and a list of axes\n",
    "\n",
    "fig7.subplots_adjust(hspace=0)       #no vertical space between subplots\n",
    "\n",
    "x7=np.linspace(-2*np.pi, 2*np.pi, 201)\n",
    "\n",
    "colors=['red', 'blue', 'orange', 'purple']\n",
    "\n",
    "for i in range(nrows):\n",
    "    n = nrows - i      #useful index: n=1 for bottom, n=nrows for the top\n",
    "    axes[i].plot(x7, np.cos(n*x7), color=colors[i%len(colors)])\n",
    "    axes[i].set_ylim(-1.25, 1.25)\n",
    "    axes[i].set_ylabel(r'$\\cos \\;('+str(n)+'x)$', fontsize=14)\n",
    "    \n",
    "    if i == nrows -1:\n",
    "        axes[i].set_xlabel(r'$x$', fontsize=14)\n",
    "    \n",
    "plt.show()\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that matplotlib also has a range of more complicated tools for subplot placement (GridSpec, etc.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
