# -*- coding: utf-8 -*-
"""


    
"""
import os
import sys
import glob
# […]

# Libs 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import math 
import scipy
from scipy import stats
import scipy.optimize as optimization

__author__ = 'Matthew Fenlon'
__email__ = 'mfenlon@tcd.ie'
__student_id__ = '17328434'

# Printing out my details 
print (__author__)
print (__email__)
print(__student_id__)


# Functions 

def plot_intro (t,number_decays):
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    line = ax1.plot(t,number_decays, markersize=.01, )
    points = ax1.plot(t,number_decays,'o',label = 'Number of Decays' )
    ax1.set_xlabel('Times (seconds)', fontsize = 14)
    ax1.set_ylabel(r'Number of Decays',fontsize = 14 )
    plt.title('Number of Decays as a Function of Time', fontsize = 'small')
    plt.xlim([0,125 ])
    plt.ylim([0, 35])
    plt.legend(loc='upper right')
    plt.savefig('Plot of Measurements.png', dpi=800)
    plt.show()

def plot_intro_2 (t,number_decays):
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    t_transformed = np.log(t)
   
    forward_line = ax1.semilogx(t_transformed,number_decays, markersize=.01 )
    points = ax1.plot(t_transformed,number_decays,'o',label = 'Number of Decays' )
    ax1.set_xlabel('$\\ln(Time)$', fontsize = 14)
    ax1.set_ylabel(r'Number of Decays',fontsize = 14 )
    plt.title('Roughly "STRAIGHT LINE"', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([0, 35])
    plt.legend(loc='upper right')
    plt.savefig('Roughly STRAIGHT LINE .png',dpi=800)
    plt.show()
    
def plot_linear_fit (t,number_decays):
    t_transformed = np.log(t)
    # array of values to be ploted (generated using fit parameters)
    s =  fit_parameters(t_transformed, number_decays)[1]*t_transformed + fit_parameters(t_transformed, number_decays)[0]
    
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    forward_line = ax1.plot(t,s, '--',markersize=.01 )
    points = ax1.plot(t,s,'o',label = 'Number of Decays' )
    points_data = ax1.plot(t,number_decays,'o',label = 'Observations' )
    ax1.set_xlabel('Times (seconds)', fontsize = 14)
    ax1.set_ylabel(r'Number of Decays',fontsize = 14 )
    ax1.axhline(y=0, color='k')
    ax1.axvline(x=0, color='k')

    plt.title('Linear Fit of Data (Own Functions)', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([-5, 35])
    plt.legend(loc='upper right')
    plt.savefig('Linear Fit of Data .png',dpi=800)
    plt.show()    
    
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    forward_line = ax2.semilogx(t,s,'--' ,markersize=.01 )
    points = ax2.plot(t,s,'o',label = 'Number of Decays' )
    points_data = ax2.plot(t,number_decays,'o',label = 'Observations' )
    ax2.set_xlabel('$\\ln(Time)$', fontsize = 14)
    ax2.set_ylabel(r'Number of Decays',fontsize = 14 )
    ax2.axhline(y=0, color='k')
    ax2.axvline(x=0, color='k')

    plt.title('Linear Fit of Data (Own Functions)', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([-5, 35])
    plt.legend(loc='upper right')
    plt.savefig('Linear Fit of Data , log x-axis .png',dpi=800)
    plt.show()  
    
    
    
    print("Estimated coefficients: Using Custom Implementation \na = {}   \nb = {}".format(fit_parameters(t_transformed, number_decays)[0], fit_parameters(t_transformed, number_decays)[1])) 
 
    
    
def plot_inital_non_linear_fit (t,number_decays):
    
    t_transformed = np.log(t)
    # array of values to be ploted (generated using fit parameters)
    s =  fit_parameters(t_transformed, number_decays)[1]*t_transformed + fit_parameters(t_transformed, number_decays)[0]
    
    # Using fit paramteters to plot corresponding decay function 
    N_0 =  fit_parameters(t_transformed, number_decays)[0]
    lamba_1 = -np.log( s/N_0)/t
    lamba_1a = lamba_1.mean()
    
    
    
    sss = N_0 * np.exp(-lamba_1a*t)
        
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    #forward_line = ax1.plot(t,s, markersize=.01 )
    points = ax1.plot(t,s,label = 'Number of Decays previous fit' )
    
    plt.plot(t, sss, label=' decay function ')
    #plt.plot(t, sss)
    points_data = ax1.plot(t,number_decays,'or',label = 'Observations' )
    
    ax1.set_xlabel('Times (seconds)', fontsize = 14)
    ax1.set_ylabel(r'Number of Decays',fontsize = 14 )
    ax1.axhline(y=0, color='k')
    ax1.axvline(x=0, color='k')

    plt.title('Non Linear fit (Own Functions)', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([-5, 35])
    plt.legend(loc='upper right')
    plt.savefig('Inital Non Linear fit of Data .png',dpi=800)
    plt.show()   
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    forward_line = ax2.semilogx(t, sss,markersize=.01 ,label=' decay function ')
    points = ax2.plot(t,s,label = 'Number of Decays' )
    points_data = ax2.plot(t,number_decays,'or',label = 'Observations' )
    ax2.set_xlabel('$\\ln(Time)$', fontsize = 14)
    ax2.set_ylabel(r'Number of Decays',fontsize = 14 )
    ax2.axhline(y=0, color='k')
    ax2.axvline(x=0, color='k')

    plt.title('Linear Fit of Data (Own Functions)', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([-5, 35])
    plt.legend(loc='upper right')
    plt.savefig('Inital Non Linear fit of Data , log x-axis .png',dpi=800)
    plt.show()  
    
    print("Estimated coefficients: Decay Function Using Custom Implementation \nN_0 = {}   \nlambda = {}".format(N_0,lamba_1a))
    
    return  

def plot_scipy_fit (t,number_decays,a,b):
    
    s = a*np.exp(-b*t)
    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    forward_line = ax1.plot(t,s, '--',markersize=.01 )
    points = ax1.plot(t,s,'o',label = 'Number of Decays' )
    points_data = ax1.plot(t,number_decays,'o',label = 'Observations' )
    ax1.set_xlabel('Times (seconds)', fontsize = 14)
    ax1.set_ylabel(r'Number of Decays',fontsize = 14 )
    ax1.axhline(y=0, color='k')
    ax1.axvline(x=0, color='k')

    plt.title('Scipy Fit of Data ', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([-5, 35])
    plt.legend(loc='upper right')
    plt.savefig('Non-Linear Fit of Data using scipy .png',dpi=800)
    plt.show()  
    
    
    fig2 = plt.figure() #create figure 
    ax2 = fig2.add_subplot() # create a subplot 
    
    forward_line = ax2.semilogx(t,s,'--' ,markersize=.01 )
    points = ax2.plot(t,s,'o',label = 'Number of Decays' )
    points_data = ax2.plot(t,number_decays,'o',label = 'Observations' )
    ax2.set_xlabel('$\\ln(Time)$', fontsize = 14)
    ax2.set_ylabel(r'Number of Decays',fontsize = 14 )
    ax2.axhline(y=0, color='k')
    ax2.axvline(x=0, color='k')

    plt.title('Scipy Fit of Data ', fontsize = 'small')
    #plt.xlim([0,125 ])
    plt.ylim([-5, 35])
    plt.legend(loc='upper right')
    plt.savefig('Non-Linear Fit of Data using scipy log x-axis .png',dpi=800)
    plt.show()  
    
   
    
    

def fit_parameters (x , y ):
    # Number of data points / observations 
    N = np.size(x)
    mean_x = np.mean(x)
    mean_y = np.mean(y)
    
   
    #/np.sum((t-np.mean(t))**2)
    # S_xy and S_xx
    SP_xy = np.sum(y*x) - N*mean_y*mean_x 
    SS_xx = np.sum(x*x) - N*mean_x*mean_x 
    
    # Parameters to be calcualted 
    b = SP_xy / SS_xx
    a = mean_y - b*mean_x
    
    
    
    return (a , b )

def func(t, a ,b ):
    return  a*np.exp(-b*t)
    
# Main 






def main(): 
    # observations 
    t = np.array([5,15,25,35,45,55,65,75,85,95,105,115])
    number_decays = np.array([32,17,21,7,8,6,5,3,4,1,5,1])
    
    plot_intro (t,number_decays)   # first normal plot 
    plot_intro_2 (t,number_decays) #second plot " STRAIGHT LINE" 
    
    # Linear regression/least square fit  
    plot_linear_fit (t,number_decays)
    plot_inital_non_linear_fit (t,number_decays)
    
    
    # Initial guess of fit parameters.
    x0 = np.array([35.983 ,-.1 ])
    # define your nonlinear function (containing two fit parameter)
    
    
    fitparameter=optimization.curve_fit(func,t,number_decays ,x0)[0] # does non- linear fit 
    a = fitparameter[0] # assigns paratmers generated by the non linear fit 
    b = fitparameter[1]
    
    plot_scipy_fit (t,number_decays,a,b)
  
    
    print("Estimated coefficients: Using scipy \na = {}   \nb = {}".format(a, b)) 
  

  
if __name__ == "__main__": 
    main()     
    

