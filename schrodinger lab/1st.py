"""
"""
# Built-in/Generic Imports
import os
import sys
import glob
# […]

# Libs 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt 
import math 
from scipy.integrate import simps 
# […]

# Own modules
#from {path} import {class}
# […] 


# Task 2 # 
# Find the wave psi numerically . 
# => In the infinite square well ( for a given non-dimensional trial energy e )
# NOTE : e >= -1 by defenition 


def psi_function(eps,N,k_2):
    """
    This function finds numerically the wavefunctions for the square well potenrial 

    Parameters
    ----------
    eps : int 
        The trial energy 
    N : int
        Is the number of iterations ( number of points that will be calculated)

    Returns
    -------
    psi : array of float64
        Cotains N values for the wave function . 

    """
    
    k_2 = gamma_sq * (eps - potential )
    
    # Need to indicate starting values 
    psi[0] = 0
    psi[1] = 1e-4 
    
    # Numerov Algorithm 
    ## Noted : looping over a range using a for loop is faster then a while loop 
    
    for i in range(2 ,N):  # does up to N-1 . 
        psi[i] = (2 * (1 - (5/12) * (l**2) *k_2[i]) * psi[i-1] - (1 + (1/12) * (l**2) * k_2[i-2]) * psi[i-2])/(1 + (1/12) * (l**2) * k_2[i])
        #plt.plot(i, psi[i])
    #plt.plot(psi)
    print (eps )
    print ("a")
    return psi

# produce a few graphs for varying eps values close to the ground state ( that was obtained from analytical solution )
#psi_function(eps,N)

# shooting algo / method 

def eigenstates_function(eps ,deps )  :
    k_2 = gamma_sq * (eps - potential )
    psi_function(eps,N,k_2) 
    P1 = psi[N-1]
    eps =  eps + deps 
    print(eps)
    
    while abs (deps) > 1e-12: # sets the tolarance ( i.e. accuracy of the function)
       k2 = gamma_sq * (eps - potential )
       psi_function(eps , N,k_2)
       P2 = psi[N-1]
       
       if P1 * P2 < 0 : # If there is a sign change 
           deps = - deps/2 
           
       eps = eps + deps 
       P1 = P2 
    print ( "eps :" )
    print ('%-10s' % eps)
    
    return eps , P1 , P2 , k_2  # returns the eigenstate 

def plot_task_3 (psi):
    """
    Plots the wavefunction 

    Parameters
    ----------
    psi : array 
        Cotains N values for the wave function .

    Returns
    -------
    None.

    """
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    data_line = ax1.plot(psi ,label = 'n = 1' )
    ax1.set_xlabel('$\widetilde{x}$', fontsize = 14)
    ax1.set_ylabel(r'${\psi}_n(\widetilde{x})$',fontsize = 14 )
    plt.title('Non-Normalised Eigenstate : ${\epsilon}_1  $ (Infinite Square Well)', fontsize = 'small')
    plt.xlim([0, N])
    plt.ylim([0, psi.max() +.005])
    plt.legend(loc='upper right')
    #plt.savefig("High resoltion.png",dpi=300)
    #fig1.savefig('myimage.png', format='png', dpi=1200)
    plt.show()

def normalize(psi):
    """
    Finds the constanted needed to normalise psi 
    Parameters
    ----------
    psi : array 
        Cotains N values for the wave function .

    Returns
    -------
    c : int
        constant needed to normalise psi (psi/c = normalised_psi)

    """
    c = simps( psi, dx=l ) # constant to normailse 
    #psi/c 
    return c
    
def plot_normalized (psi):
    """
    Plots the wavefunction 

    Parameters
    ----------
    psi : array 
        Cotains N values for the wave function .

    Returns
    -------
    None.

    """
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
    normalised_psi = psi/normalize(psi)
   
    data_line = ax1.plot(normalised_psi ,label = 'n = 4' )
    ax1.set_xlabel('$\widetilde{x}$', fontsize = 14)
    ax1.set_ylabel(r'${\psi}_n(\widetilde{x})$',fontsize = 14 )
    plt.title('Normalised Eigenstate : ${\epsilon}_4  $ (Infinite Square Well)', fontsize = 'small')
    plt.xlim([0, N])
    plt.legend(loc='upper right')
    #plt.savefig("High resoltion.png",dpi=300)
    fig1.savefig('myimage.png', format='png', dpi=1200)
    plt.show()    

# Parameters  

N = 1000 # number of iterations (i.e the number of values to be stored) 
psi = np.zeros(N) # array , that will store the values for the wave function 
eps = -0.95
l = 1/(N-1) 

gamma_sq = 200 # gamma squared , set to a chosen value 
potential = -np.ones(N) # stores the value for the potential . Array of N -1's 

k_2 = gamma_sq * (eps - potential )       
#x  = eigenstates_function(-0.99,.08)        

# produce array psi with correct values 
eigenstates_function(-.22,.01)  

# graphs psi for given value of interest 
#plot_task_3(psi)

# normalize the array 
#normalized_psi = normalize(psi)

# plot non - normalized wavefunction 
#plot_task_3(psi)

# plot normalized wavefunction
#plot_normalized (psi)

normalize(psi)

def psi_2 (epsilon,psi): #2nd derivaitve 
    psi_2 = np.zeros(N)
    for n in range (1,N-1): # loops through each term in phi from 0 and N-1 
        psi_2[n] = (psi[n-1]-(2*psi[n]) +psi[n+1])(l**2) 
    
    return psi_2
    

def uncertainty (epsilon,psi_1): # Uncertainty function 
    x_1 = simps(x)(x*norm(epsilon,psi_1)**2,dx = l) # finds <x^1>
    x_2 = simps( (x**2)*norm(epsilon,psi_1**2 ), dx = l )
    deltax = np.sqrt(x_2 - (x_1**2)) 
    deltap = np.sqrt( -simps(norm(epsilon, psi_1)*psi_2(epsilon , norm (epsilon,psi_1)) , dx = l  ))
    
    return deltax*deltap 

"""
def plot_task_3 (psi):
   
    Plots the wavefunction 

    Parameters
    ----------
    psi : array 
        Cotains N values for the wave function .

    Returns
    -------
    None.

    
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    
   
    data_line = ax1.plot(psi  )
    ax1.set_xlabel('$\widetilde{x}$')
    ax1.set_ylabel(r'${\psi}_n(\widetilde{x})$' )
    plt.title('Non-Normalised Eigenstate : ${\epsilon}_n  $ (Infinite Square Well)', fontsize = 'small')
    plt.xlim([0, 1000])
    #plt.ylim([0]
    plt.legend(loc='upper right')
    plt.savefig("e_10.png",dpi=1200,bbox_inches='tight')
    #fig1.savefig('myimage.png', format='png', dpi=1200)
    plt.show()
    
    
def t (list_e):
    fig1 = plt.figure() #create figure 
    ax1 = fig1.add_subplot() # create a subplot 
    for i in range (len(list_e)) :
         plt.plot(list_e[i])
    ax1.set_xlabel('$\widetilde{x}$')
    ax1.set_ylabel(r'${\psi}_n(\widetilde{x})$' )
    plt.title('Non-Normalised Eigenstate : ${\epsilon}_n  $ (Infinite Square Well)', fontsize = 'small')
    plt.xlim([0, 1000])
    #plt.ylim([0]
    plt.savefig("e_n .png",dpi=1200,bbox_inches='tight')
    #fig1.savefig('myimage.png', format='png', dpi=1200)
    plt.show()


"""


