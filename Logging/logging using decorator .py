
from functools import wraps
import logging

def log_shape(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        logging.info("%s,%s" % (func.__name__, result.shape))
        print(f'just ran step {func.__name__} ')
        return result
    return wrapper

def log_dtypes(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        logging.info("%s,%s" % (func.__name__, result.dtypes))
        return result
    return wrapper


@log_shape
@log_dtypes
def load_1(fp):
    df = pd.read_csv(str(fp))
    return df

@log_shape
@log_dtypes
def update_events(df, new_events):
    df.loc[new_events.index, ' country'] = new_events
    return df
